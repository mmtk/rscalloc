# Copyright (c) 2017, The Australian National University.  All rights reserved.
# Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
# Please see the AUTHORS file for details.  Use of this source code is governed
# by a BSD license that can be found in the LICENSE file.\n
cargo +nightly rustc --release -- -C llvm-args='-ffast-math' -C target-cpu=native
