<!--
Copyright (c) 2017, The Australian National University.  All rights reserved.
Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
Please see the AUTHORS file for details.  Use of this source code is governed
by a BSD license that can be found in the LICENSE file.
-->

Rust port of scalloc, based on the Original C++ version of scalloc (<https://github.com/cksystemsgroup/scalloc>).

Note: unlike the latter, this version of scalloc does not currently support macOS.

# Usage
* To build run `build.sh`, or manually using the nightly bersion of `cargo`/`rustc`
* To use:
 	* build your program against `./target/release/librscalloc.so` or `./target/release/librscalloc.a`
 	* Or run any program with the `LD_PRELOAD` environemnt variable set to include `./target/release/librscalloc.so`
* Use the functions (defined in `./lib.rs`) starting with `scalloc_` to force use of scalloc, and prevent your allocator
being overriden by another library

# Feature flags
* Use the `--features "feature1 feature2"` flag to Cargo to sellect from the folowing features:
	* `default` (on by default, use `--no-default-features` to exclude): this just includes the `override` and `core` features
	* `override`: override the posix-memory allocator functions (e.g. `malloc`), without this you have to use the `scalloc_` variants (e.g. `scalloc_malloc`) to use the allocator
	* `trace` (implies `debug`): print *a lot* more verbose debug information
	* `debug` (implies `release`): print some debugging information and perform runtime-checks
	* `core` (REQUIRED): provides core features

# Build Requirements:
* Nightly Rust toolchain (version 1.23.0 - 1.25.0, obtainable from <https://rustup.rs/>)
	
# Runtime Requirements:
* x86-64 Linux (requires at least a Pentium P5 or later, tested on kernels version 3.13.0 and 4.14.13)
* Memory overcommiting and transparent huge pages needs to be enabled in the kernel:
	```
	sudo sh -c "echo 1 > /proc/sys/vm/overcommit_memory"
	sudo sh -c "echo never > /sys/kernel/mm/transparent_hugepage/enabled"
	```

# Copyright
Copyright (c) 2017, The Australian National University.  All rights reserved.

Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.

Please see the AUTHORS file for details.  Use of this source code is governed by a BSD license that can be found in the LICENSE file.
