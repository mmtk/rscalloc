#![feature(allocator_api)]
#![feature(alloc)]
#![feature(global_allocator)]
#![feature(const_fn)]
#![feature(pointer_methods)]
#![feature(thread_local)]

extern crate libc;
extern crate alloc;
use self::alloc::heap::{Alloc, AllocErr, Layout};
use std::ptr::null_mut;

const PAGE_SIZE: usize = 4096;

#[global_allocator]
pub static MMAP_ALLOC: MmapAllocator = MmapAllocator();
pub struct MmapAllocator();

#[thread_local]
static mut ALLOC_BUFFER: AllocatorBuffer = AllocatorBuffer::new();
struct AllocatorBuffer{ptr: *mut u8, end: *mut u8}
impl AllocatorBuffer{
    const fn new() -> AllocatorBuffer {
        AllocatorBuffer{ ptr: null_mut(), end: null_mut() }
    }
}

unsafe impl Sync for MmapAllocator {}
unsafe impl Send for MmapAllocator {}

unsafe impl Alloc for MmapAllocator {
    // Necesary functions
    #[inline] unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
        (&*self).alloc(layout)
    }
    #[inline] unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        (&*self).dealloc(ptr, layout)
    }
}

#[inline]
fn pad_size(size: usize, multiple: usize) -> usize {
    (size + multiple - 1) / multiple * multiple
}

unsafe impl<'a> Alloc for &'a MmapAllocator {
    // Necesary functions
    #[inline] unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
        let mut res = pad_size(ALLOC_BUFFER.ptr as usize, layout.align());
        let mut new_ptr = res + layout.size();
        // We don't have enough mmaped memory, so mmap some more
        if ALLOC_BUFFER.ptr.is_null() || new_ptr > ALLOC_BUFFER.end as usize {
            // How much memory to mmap
            let s = pad_size(layout.size(), PAGE_SIZE) + layout.align();
            ALLOC_BUFFER.ptr = libc::mmap(null_mut(), s,
                libc::PROT_READ | libc::PROT_WRITE,
                libc::MAP_PRIVATE | libc::MAP_ANONYMOUS, -1, 0) as *mut u8;
            if ALLOC_BUFFER.ptr.is_null() || ALLOC_BUFFER.ptr == libc::MAP_FAILED as *mut u8 {
                return Err(AllocErr::Exhausted { request: layout })
            }
            ALLOC_BUFFER.end = ALLOC_BUFFER.ptr.add(s);

            res = pad_size(ALLOC_BUFFER.ptr as usize, layout.align());
            new_ptr = res + layout.size();
        }

        ALLOC_BUFFER.ptr = new_ptr as *mut u8;
        Ok(new_ptr as *mut u8)
    }
    #[inline] unsafe fn dealloc(&mut self, __ptr: *mut u8, __layout: Layout) { /*Do nothing*/ }
}

