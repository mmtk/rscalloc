// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[repr(C)]
pub struct Arena {
    name: CharPtr,

    //: All of the following members hold pointers to their respective locations.
    //: Use unsigned to make comparison against size_t easy.
    start: UIntPtr,
    end: UIntPtr,
    len: UIntPtr,

    __pad1: [u8; 64 - (size_of::<CharPtr>() + 3*size_of::<UIntPtr>())%64],

    current: AtomicUIntPtr,
    __pad2: [u8; 64 - size_of::<AtomicUIntPtr>()%64],
}

impl Arena {
    //: Globally constructed, hence we use staged construction.
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub const fn new()->Arena {
        //TODO: unsafe{uninitialized()}
        Arena {
            name: CharPtr::null(),
            start: 0,
            end: 0,
            len: 0,
            __pad1: [0; 64 - (size_of::<CharPtr>() + 3*size_of::<UIntPtr>())%64],
            current: AtomicUIntPtr::new(0),
            __pad2: [0; 64 - size_of::<AtomicUIntPtr>()%64],
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn init(&mut self, size: usize, alignment: usize, name: CharPtr) {
        self.name = name;
        self.len = size;

        self.start = system_mmap_guided(Address::from(alignment).into(), size).into();
        let needs_aligning = self.start == 0;

        if needs_aligning {
            self.start = system_mmap(size + alignment).into();
        }

        if self.start == 0 {
            fatal!("initial mmap of arena failed. consult online docs for requirements \
                 (overcommit_memory)");
        }

        if needs_aligning {
            self.start += alignment - (self.start % alignment)
        }

        scalloc_assert_eq!(self.start % alignment, 0);
        self.end = self.start + size;
        self.current.store(self.start, Ordering::SeqCst);

        if cfg!(feature = "SCALLOC_STRICT_DUMP") {
            unsafe{libc::madvise(Address::from(self.start).into(), self.len, libc::MADV_DONTDUMP)};
        }

        trace!("arena at ", Address::from(self));
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn contains(&self, p: OptAddress) -> bool {
        //: Requires proper alignment of start_!
        likely((p ^ self.start).to_unsigned() < self.len)
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn allocate(&mut self, size: usize) -> Address {
        trace!("allocate: ", size);
        let obj = self.current.fetch_add(size, Ordering::SeqCst);
        if unlikely((obj + size) >= self.end) {
            fatal!(self.name,
                " arena OOM; start: ", Address::from(self.start),
                ", end ", Address::from(self.end),
                ", curr: ", Address::from(self.current.load(Ordering::SeqCst)));
        }
        trace!(self.name, ": obj: ", Address::from(obj));

        if cfg!(feature = "SCALLOC_STRICT_DUMP") {
            unsafe {
                libc::madvise(
                    Address::from(obj & PAGE_NR_MASK as UIntPtr).into(),
                    ((size / PAGE_SIZE) + 1) * PAGE_SIZE,
                    libc::MADV_DODUMP,
                )
            };
        }
        Address::from(obj).into()
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn allocate_virtual_span(&mut self) -> Address {
        trace!("allocate: ", VIRTUAL_SPAN_SIZE);
        let obj = self.current.fetch_add(VIRTUAL_SPAN_SIZE, Ordering::SeqCst);
        if unlikely((obj + VIRTUAL_SPAN_SIZE) >= self.end) {
            fatal!(self.name,
                " arena OOM; start: ", Address::from(self.start),
                ", end ", Address::from(self.end),
                ", curr: ", Address::from(self.current.load(Ordering::SeqCst)));
        }
        trace!(self.name, ": obj: ", Address::from(obj));

        if cfg!(feature = "SCALLOC_STRICT_DUMP") {
            unsafe { libc::madvise(Address::from(obj).into(), VIRTUAL_SPAN_SIZE, libc::MADV_DODUMP) };
        }
        Address::from(obj)
    }
}
