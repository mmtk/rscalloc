// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;
// Handles pointer operations for us
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Address(Shared<Void>);
impl Address {
    #[inline(always)]
    pub fn try_new<T>(value: *const T) -> Option<Address> {
        // Hope this is just a MOV...
        Shared::new(value as *mut Void).map_or(None, |x| Some(Address(x)))
    }

    #[inline(always)]
    fn new<T>(value: *const T) -> Address { unwrap!(Address::try_new(value)) }

    #[inline(always)]
    pub fn to_ptr<T>(self) -> *mut T { self.0.as_ptr() as *mut T }
    address_methods!();
}
impl Size64 for Address { size_64_impl!{} }

address_impl!(Address);

impl<T> Into<Shared<T>> for Address {
    fn into(self)->Shared<T> {
        // SAFE: Addresse's arn't supposed to ever be zero
        unsafe{Shared::new_unchecked(self.to_ptr())}
    }
}