// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn malloc(size: usize)->OptAddress {
    trace!("malloc: size: ", size);
    let obj = unsafe{AB_SCHEDULER.get_ab().as_mut()}.allocate(size);
    trace!("returning ", obj);
    //: errno is set in a slow path as soon as we know we cannot serve the request.
    obj
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn free(p: OptAddress) {
    #[cfg(feature = "RUST_PANIC_HACK")] {
        if unlikely(unsafe{SCALLOC_PANICKING}) {
            return;
        }
    }

    //: No need to check whether p is NULL here since it will fall through the fast
    //: path anyways.

    if likely(unsafe{OBJECT_SPACE.contains(p)}) {
        // P can't be null
        unsafe{AB_SCHEDULER.get_ab().as_mut()}.free(unwrap!(p));

    //: We are in the path for super large objects. Check for NULL here.
    } else if likely(!p.is_null()) {
        LargeObject::free(unwrap!(p));
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn calloc(nmemb: usize, size: usize)->OptAddress {
    #[cfg(feature = "RUST_PANIC_HACK")] {
        if unlikely(unsafe{SCALLOC_PANICKING}) {
            return system_mmap(size); // This works as mmap zeroes memory
        }
    }

    trace!("calloc: size: ", size);
    let malloc_size = nmemb*size;
    if (size != 0) && (malloc_size / size) != nmemb {
        OptAddress::null()
    } else {
        let result = malloc(malloc_size);         //: malloc() sets errno
        if !result.is_null() {
            unsafe{result.to_ptr::<Byte>().write_bytes(0, malloc_size)}
        }
        result
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn realloc(ptr: OptAddress, size: usize)->OptAddress {
    if unlikely(ptr.is_null()) {
        malloc(size)
    } else {
        #[cfg(feature = "RUST_PANIC_HACK")] {
            if unlikely(unsafe{SCALLOC_PANICKING}) {
                abort();
            }
        }

        let ptr = unwrap!(ptr);
        let old_size = if likely(unsafe{OBJECT_SPACE.contains(ptr.into())}) {
            let s = Span::from_object(ptr);
            CLASS_TO_SIZE.val_index(s.size_class()) as usize
        } else {
            LargeObject::payload_size_ptr(ptr)
        };

        if old_size >= size {
            ptr.into()
        } else {
            let new_obj = malloc(size);
            if !new_obj.is_null() {
                unsafe {ptr.to_ptr::<Byte>().copy_to(new_obj.into(), old_size)};
                free(ptr.into());
            }
            new_obj
        }
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn posix_memalign(ptr: &mut OptAddress, align: usize, size: usize)->Int {
    trace!("posix memalign: size: ", size);
    if unsafe{SEEN_MEMALIGN == 0} {
        unsafe{SEEN_MEMALIGN = 1};
    }

    //: Return free-able pointer for size 0.
    if unlikely(size == 0) {
        *ptr = OptAddress::null();
        0
    } else {
        let size_needed = align + size;

        //: TODO: check align for power of 2
        //: TODO: check size_needed for overflow

        let start = malloc(size_needed);
        if unlikely(start.is_null()) {
            libc::ENOMEM
        } else {
            let new_start = Address::from(start + align - (start % align));
            if new_start != start {
                //: We add a magicnumber to force recalculation of free() address. This is
                //: valid because we only transition into a slow path but handle the
                //: free still correct.
                *unsafe{(new_start - size_of::<u32>()).to_mut::<u32>()} = 0xAAAAAAAA;
            }

            *ptr = new_start.into();
            0
        }
    }
}


#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn memalign(alignment: usize, size: usize)->OptAddress {
    // SAFE: we only use the value if posix_memalign returns 0, in which case it will be asigned
    let mut mem: OptAddress = unsafe{uninitialized()};
    if posix_memalign(&mut mem, alignment, size) != 0 {
        OptAddress::null()
    } else {
        mem
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn aligned_alloc(alignment: usize, size: usize)->OptAddress {
    //: The function aligned_alloc() is the same as memalign(), except for the
    //: added restriction that size should be a multiple of alignment.
    if size % alignment != 0 {
        unsafe{*libc::__errno_location() = libc::EINVAL};
        OptAddress::null()
    } else {
        memalign(alignment, size)
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn valloc(size: usize)->OptAddress { memalign(PAGE_SIZE, size) }

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub(crate) fn pvalloc(size: usize)->OptAddress { memalign(PAGE_SIZE, pad_size(size, PAGE_SIZE)) }


#[inline]
pub(crate) fn malloc_stats() { }

#[inline]
pub(crate) fn mallopt(__cmd: Int, __valu: Int)->Int { 0 }

// glue.cc //

// size_classes:
const fn number_of_objects(__a: i32, __b: i32, __c: usize, d: usize)->i32 { d as i32 }
pub static CLASS_TO_OBJECTS: [i32; SIZE_CLASS_COUNT] = for_all_size_classes!(number_of_objects);

const fn object_size(__a: i32, b: i32, __c: usize, __d: usize)->i32 { b }
pub static CLASS_TO_SIZE: [i32; SIZE_CLASS_COUNT] = for_all_size_classes!(object_size);

#[allow(unused)]
const fn span_size(__a: i32, __b: i32, c: usize, __d: usize)->i32 { c as i32 }
#[allow(unused)]
pub static CLASS_TO_SPAN_SIZE: [i32; SIZE_CLASS_COUNT] = for_all_size_classes!(span_size);

const fn reuse_th(__a: i32, __b: i32, __c: usize, d: usize)->i32 { (d as i32*REUSE_THRESHOLD)/100 }
pub static CLASS_TO_REUSE_THRESHOLD: [i32; SIZE_CLASS_COUNT] = for_all_size_classes!(reuse_th);

// globals:
//: Be careful with order here! Since we define all globals in a single
//: translation unit we can rely on order.
pub(crate) static mut CORE_SPACE: CacheAligned<Arena> = CacheAligned::new(Arena::new());
pub(crate) static mut OBJECT_SPACE: CacheAligned<Arena> = CacheAligned::new(Arena::new());
pub(crate) static mut SPAN_POOL: CacheAligned<SpanPool> = CacheAligned::new(SpanPool::new());
pub(crate) static mut AB_SCHEDULER: CacheAligned<ABProvider> = CacheAligned::new(ABProvider::new());

// glue:
//static mut STARTUP_EXIT_HOOK: CacheAligned<ScallocGuard> = CacheAligned::new(ScallocGuard::new());
pub(crate) static mut SCALLOC_GUARD_REF_COUNT: /*:cache_aligned*/ i32 = 0;

// core/glue
pub static mut SEEN_MEMALIGN: /*:cache_aligned*/ i32 = 0;

// span:
#[cfg(feature = "PROFILE")]
pub(crate) static mut LOCAL_FREES: AtomicI32 = AtomicI32::new(0);
#[cfg(feature = "PROFILE")]
pub(crate) static mut REMOTE_FREES: AtomicI32 = AtomicI32::new(0);


// 7:00 ... 7:58
// 8:31 -

pub extern "C" fn exit_handler() {
    #[cfg(feature = "profile")] {
        unsafe{SPAN_POOL.print_profile_summary()};
        warning!("free summary: local: ", unsafe{LOCAL_FREES.load(Ordering::SeqCst)},
            ", remote: ", unsafe{REMOTE_FREES.load(Ordering::SeqCst)});
    }
}

pub(crate) fn scalloc_init() {
    unsafe{CORE_SPACE.init(LAB_SPACE_SIZE as usize, PAGE_SIZE, CharPtr::from_str("LAB\0"))};
    unsafe{OBJECT_SPACE.init(OBJECT_SPACE_SIZE as usize, OBJECT_SPACE_SIZE as usize,
        CharPtr::from_str("object\0"))};
    unsafe{SPAN_POOL.init()};
    unsafe{AB_SCHEDULER.init()};

    unsafe{AB_SCHEDULER.get_me_alab()};
    replace_system_allocator();
    // TODO: Do something more 'Rusty' (I'm not entirely sure how this interacts with Rust code,
    // e.g. does std::process::exit call this function? What about panics?)
    unsafe{libc::atexit(exit_handler)};
}

// class ScallocGuard::ScallocGuard()
extern "C" fn scalloc_guard() {
    trace!("initilising scalloc!");
    // No ++ operator in Rust,
    let r = unsafe{SCALLOC_GUARD_REF_COUNT};
    unsafe{SCALLOC_GUARD_REF_COUNT += 1};
    if r == 0 {
        scalloc_init()
    }
}

#[used]
#[link_section = ".init_array"]
static INIT_ARRAY: [extern "C" fn(); 1] = [scalloc_guard];
