// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub(crate) type CoreId = TaggedValue<*mut Core>;
pub(crate) type AtomicCoreID = AtomicTaggedValue<*mut Core>;

// UNSAFE: this is only used so that TERMINATED can be declared a static const
// TERMINATED is safe, as it is just a null-pointer (but non-null pointers may not be safe
// as reading/writing to them could cause data races)
unsafe impl Sync for CoreId {}

//CoreId::new(null_mut(), 0)
pub(crate) static TERMINATED: CacheAligned<CoreId> = CacheAligned::new(CoreId::default());
