// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn pad_size(size: usize, multiple: usize) -> usize {
    (size + multiple - 1) / multiple * multiple
}


#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn system_mmap_guided(hint: Address, size: usize) -> OptAddress {
    let flags = libc::MAP_PRIVATE | libc::MAP_ANONYMOUS;
    let prot = libc::PROT_READ | libc::PROT_WRITE;

    let p = OptAddress::from(unsafe{libc::mmap(hint.into(), size, prot, flags, -1, 0)});
    if unlikely(p.to_ptr() == libc::MAP_FAILED) {
        OptAddress::null()
    } else if hint != p {
        unsafe{libc::munmap(p.into(), size)};
        OptAddress::null()
    } else {
        if cfg!(feature = "SCALLOC_DISABLE_TRANSPARENT_HUGEPAGES") {
            if unsafe{libc::madvise(p.into(), size, libc::MADV_NOHUGEPAGE)} != 0 {
                fatal!("madvise MADV_NOHUGEPAGE failed")
            }
        }

        p
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn system_mmap(size: usize) -> OptAddress {
    let flags = libc::MAP_PRIVATE | libc::MAP_ANONYMOUS;
    let prot = libc::PROT_READ | libc::PROT_WRITE;
    let p = OptAddress::from(unsafe{libc::mmap(null_mut(), size, prot, flags, -1, 0)});

    if unlikely(p.to_ptr() == libc::MAP_FAILED) {
        OptAddress::null()
    } else {
        if cfg!(feature = "SCALLOC_DISABLE_TRANSPARENT_HUGEPAGES") {
            if unsafe { libc::madvise(p.into(), size, libc::MADV_NOHUGEPAGE) } != 0 {
                fatal!("madvise MADV_NOHUGEPAGE failed")
            }
        }
        p
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn system_mmap_fail(size: usize)->Address {
    let p = system_mmap(size);
    p.unwrap_or_else(|| /*unlikely*/ fatal!("mmap failed"))
}

// We don't wan't rustfmt to make this unreadable..
#[cfg_attr(rustfmt, rustfmt_skip)]
const LOG_TABLE: [Byte; 256] = [
    -1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
];

// base-2 logarithm of 32-bit integers
#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn log2(v: usize)->Int {
    let tt = (v >> 16) as UInt;
    let r = (if tt != 0 {
        let t = (tt >> 8) as UInt;
        if t != 0 {
            24 + LOG_TABLE.val_index(t as usize)
        } else {
            16 + LOG_TABLE.val_index(tt as usize)
        }
    } else {
        let t = (v >> 8) as UInt;
        if t != 0 {
            8 + LOG_TABLE.val_index(t as usize)
        } else {
            LOG_TABLE.val_index(v as usize)
        }
    }) as UInt;
    r as Int
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn cpus_online() -> i32 {
    static mut CPUS_ONLINE: CacheAligned<i32> = CacheAligned::new(-1);

    // Needed due to use of mutable static
    unsafe {
        if *CPUS_ONLINE == -1 {
            *CPUS_ONLINE = libc::sysconf(libc::_SC_NPROCESSORS_ONLN) as i32;
            if *CPUS_ONLINE == -1 /* sysconf failed */
                || *CPUS_ONLINE == 0 /* this should not happen */ {
                fatal!("cpus_online failed");
            }
        }
        *CPUS_ONLINE
    }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn rtdsc()->UFast64 {
    let hi: UInt;
    let lo: UInt;
    // SAFE: this instruction won't do anything crazy (it literally just sets EAX and EDX)
    unsafe{ asm!("RDTSC" : "={eax}"(lo), "={edx}"(hi) : : : "volatile")};
    lo as UFast64 | ((hi as UFast64) << 32)
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn hwrand() -> UFast64 {
    rtdsc() >> 6
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
fn pseudorand()->u64 {
    //: Used for pseudorand
    const A: u32 = 16807;
    const M: u32 = 2147483647;
    const Q: u32 = 127773;
    const R: u32 = 2836;

    static mut S: i32 = 1237;
    let seed = unsafe{S};
    let hi = seed as u32/Q;
    let lo = seed as u32%Q;
    let seed = (A*lo).wrapping_sub(R*hi) as i32;
    let seed = if seed < 0 {
        (seed as u32).wrapping_add(M) as i32
    } else {
        seed
    };
    unsafe{S = seed};
    seed as u64
}