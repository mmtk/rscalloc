// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub const PAGE_SIZE: usize = 4096;
pub const PAGE_NR_MASK: u64 = !(PAGE_SIZE as u64 - 1);
//pub const PAGE_OFFSET_MASK: u64 = PAGE_SIZE as u64 - 1;

pub const MAX_THREADS: usize = 128;

pub const KILO: u64 = 1 << 10;
pub const MEGA: u64 = KILO * KILO;
pub const GIGA: u64 = MEGA * KILO;
pub const TERA: u64 = GIGA * KILO;

pub const LAB_SPACE_SIZE: u64 = (100 * PAGE_SIZE) as u64;
pub const OBJECT_SPACE_SIZE: u64 = 35 * TERA;

//: TODO: Cleanup.
pub const MAX_SMALL_SHIFT: usize = 8; // 256B
pub const MAX_MEDIUM_SHIFT: usize = 20; // 1MiB
pub const VIRTUAL_SPAN_SHIFT: usize = 21; // 2MiB

pub const MIN_ALIGNMENT: usize = 16;
pub const MAX_SMALL_SIZE: usize = 1 << MAX_SMALL_SHIFT;
pub const MAX_MEDIUM_SIZE: usize = 1 << MAX_MEDIUM_SHIFT;
pub const VIRTUAL_SPAN_SIZE: usize = 1 << VIRTUAL_SPAN_SHIFT;
pub const VIRTUAL_SPAN_MASK: UIntPtr = !(VIRTUAL_SPAN_SIZE - 1) as UIntPtr;
pub const FINE_CLASSES: usize = MAX_SMALL_SIZE / MIN_ALIGNMENT + 1;
pub const COARSE_CLASSES: usize = MAX_MEDIUM_SHIFT - MAX_SMALL_SHIFT;
pub const NUM_CLASSES: i32 = (FINE_CLASSES + COARSE_CLASSES) as i32;

//: Some defaults.
//dflag!(SCALLOC_NO_MADVISE, 1);
//dflag!(SCALLOC_NO_MADVISE_EAGER, 1);

pub const REUSE_THRESHOLD: i32 = dflag!(SCALLOC_REUSE_THRESHOLD = 80);

//dflag!(SCALLOC_LAB_MODEL, SCALLOC_LAB_MODEL_TLAB)
#[cfg(feature = "SCALLOC_LAB_MODEL_TLAB")]
pub(crate) type ABProvider = ThreadLocalAllocationBuffer;
#[cfg(feature = "SCALLOC_LAB_MODEL_RR")]
pub(crate) type ABProvider = RoundRobinAllocationBuffer;