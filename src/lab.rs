// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub mod tls_base {
    use super::*;
    type TLSDestructor = Option<unsafe extern "C" fn(*mut Void)>;

    // Has an alignment of 4*128 (
    aligned!((
        u128, u128, u128, u128, u128, u128, u128, u128,  // 128-Bytes
        u128, u128, u128, u128, u128, u128, u128, u128,  // 128-Bytes
        u128, u128, u128, u128, u128, u128, u128, u128,  // 128-Bytes
        u128, u128, u128, u128, u128, u128, u128, u128) // 128-Bytes

        #[thread_local]
        #[no_mangle]
        pub static mut AB: OptAddress = OptAddress::null()
    );

    static mut DTOR: TLSDestructor = None;

    #[repr(C)]
    pub struct TLSBase<T> {
        tls_key: libc::pthread_key_t,
        __mark: PhantomData<T>
    }
    impl<T> TLSBase<T> {
        //: Globally constructed, hence we use staged construction.
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn new()->Self { Self { tls_key: 0, __mark: PhantomData{} } }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn init(&mut self, dtor: TLSDestructor) {
            unsafe{DTOR = dtor};
            unsafe{libc::pthread_key_create(&mut self.tls_key, Some(Self::tls_base_destructor))};
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn get_tls(&self)->*mut T {
            if HAVE_TLS {
                unsafe{tls64!(AB as *mut T)}
            } else {
                OptAddress::from(unsafe{libc::pthread_getspecific(self.tls_key)}).into()
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn set_tls(&self, t: *mut T) {
            if HAVE_TLS {
                unsafe{tls64!(AB = OptAddress::from(t))};
            }

            //: Also set TSD to receive an argument in dtors.
            unsafe{libc::pthread_setspecific(self.tls_key, t as *const Void)};
        }


        #[inline]
        unsafe extern "C" fn tls_base_destructor(__t: *mut Void) {
            unwrap!(DTOR)(tls64!(AB as *mut Void));
            tls64!(AB = OptAddress::null());
        }
    }
}
use self::tls_base::TLSBase;

pub mod thread_local_allocation_buffer {
    use super::*;

    // Has an alignment of 128
    aligned!(type Aligned128(u128, u128, u128, u128, u128, u128, u128, u128));

    // Uninitialized
    static mut THREAD_IDS: Aligned128<AtomicIFast32> = Aligned128::new(AtomicIFast32::new(0));
    // Uninitialized
    static mut FREE_ABS: Aligned128<FreeAllocationBuffers> = Aligned128::new(
        FreeAllocationBuffers::new());


    type FreeAllocationBuffers = Stack128;

    #[repr(C)]
    pub(crate) struct ThreadLocalAllocationBuffer {
        pub tls_base: TLSBase<Core>
    }

    impl ThreadLocalAllocationBuffer {
        //: Globally constructed, hence we use staged construction.
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn new() -> Self { Self {tls_base: TLSBase::new() } }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn init(&mut self) {
            self.tls_base.init(Some(Self::thread_destructor));
        }

        //static inline void ThreadDestructor(void* tlab);

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn find_free_ab()->*mut Core {
            let ab = unsafe{(*FREE_ABS).pop()}.to_ptr::<Core>();
            if ab.is_null() {
                Core::new(unsafe{CORE_SPACE.allocate(size_of::<Core>()).to_mut::<Core>()})
            } else {
                ab
            }
        }


        #[inline]
        unsafe extern "C" fn thread_destructor(tlab: *mut Void) {
            let tlab = Address::from(tlab);
            trace!("Destroy at ", tlab);
            tlab.to_mut::<Core>().destroy();
            SPAN_POOL.announce_leaving_thread();
            (*FREE_ABS).push(tlab);
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn get_me_alab(&mut self) {
            let ab = self.tls_base.get_tls();
            if likely(ab.is_null()) {
                let ab = Self::find_free_ab();
                if unlikely(ab.is_null()) {
                    fatal!("reached maximum number of threads.");
                }
                unsafe{&mut *ab}.init(CoreId::new(ab,
                    (unsafe{THREAD_IDS.fetch_add(1, Ordering::SeqCst)} + 1) as tagged_value::TagType));
                self.tls_base.set_tls(ab);
                unsafe{SPAN_POOL.announce_new_thread()};
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn get_ab(&mut self)->Shared<Core> {
            let ab = self.tls_base.get_tls();
            scalloc_assert!(!ab.is_null());
            unwrap!(Shared::new(ab))
        }
    }
}

pub(crate) use thread_local_allocation_buffer::ThreadLocalAllocationBuffer;

pub mod round_robin_allocation_buffer {
    use super::*;

    // Uninitialised
    zeroed!(pub(crate) static mut ALLOCATION_BUFFERS: [GuardedCore; MAX_THREADS]);

    #[repr(C)]
    pub(crate) struct RoundRobinAllocationBuffer {
        pub tls_base: TLSBase<GuardedCore>,
        thread_counter: AtomicUFast64,
    }

    impl RoundRobinAllocationBuffer {
        #[inline]
        unsafe extern "C" fn thread_destructor(__lab: *mut Void) {  }

        //: Globally constructed, hence we use staged construction.
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn new() -> Self {
            Self {
                tls_base: TLSBase::new(),
                thread_counter: AtomicUFast64::new(0) //uninitialized()
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn init(&mut self) {
            self.tls_base.init(Some(Self::thread_destructor));
            self.thread_counter = AtomicUFast64::new(0);
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn get_ab(&mut self)->Shared<GuardedCore> {
            let ab = self.tls_base.get_tls();
            if unlikely(ab.is_null()) {
                let num_cores = cpus_online();
                let ab = unsafe{ALLOCATION_BUFFERS.mut_index((self.thread_counter.fetch_add(1,
                    Ordering::SeqCst) % num_cores as UFast64) as usize)};
                self.tls_base.set_tls(ab);
                trace!("RoundRobinAllocationBuffer: tid: ",
                    self.thread_counter.load(Ordering::SeqCst));
                ab.announce_new_thread();
                while ab.in_use() {
                    // SAFE: it's jut a pause
                    unsafe{asm!("PAUSE")};
                }
                trace!("thread_counter ", self.thread_counter.load(Ordering::SeqCst),
                    " up and ready");
                Shared::from(ab)
            } else {
                unwrap!(Shared::new(ab))
            }
        }
    }
}

pub(crate) use round_robin_allocation_buffer::RoundRobinAllocationBuffer;