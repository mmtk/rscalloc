// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
#![feature(thread_local)]
#![feature(i128_type)]
#![feature(core_intrinsics)]
#![feature(const_size_of)]
#![feature(intrinsics)]
#![feature(const_fn)]
#![feature(integer_atomics)]
#![feature(asm)]
#![feature(associated_type_defaults)]
#![feature(repr_simd)]
#![feature(linkage)]
#![feature(shared)]
#![feature(const_atomic_i32_new)]
#![feature(macro_vis_matcher)]
#![feature(used)]
#![feature(pointer_methods)]
#![feature(offset_to)]
#![feature(const_atomic_u64_new)]
#![feature(const_ptr_null_mut)]
#![feature(const_atomic_i64_new)]
#![feature(const_atomic_usize_new)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_macros)]

#[cfg(feature = "RUST_PANIC")]
extern crate mmap_allocator;
#[cfg(feature = "RUST_PANIC")]
pub use mmap_allocator::*;

extern crate libc;

use std::sync::atomic::{AtomicI32, AtomicI64, AtomicU64, Ordering};
use std::process::abort;
use std::marker::PhantomData;
use std::mem::{uninitialized, size_of, size_of_val, transmute};
use std::ptr::{null_mut, Shared, read_volatile, write_volatile};
use std::ops::{Add, AddAssign, Sub, SubAssign, Rem, BitAnd, BitOr, BitXor, Div, Neg};
use std::error::Error;
use std::{fmt, slice, str};
use std::borrow::Cow;
use std::str::Utf8Error;
use std::ffi::CString;

type Void = libc::c_void;

// Corresponds to C++ using a 'char' (but not as a character)
type Byte = libc::c_char;
// A raper over a C null-terminated byte string (i.e. const char*)
// (this is totally unsafe)
#[derive(Copy, Clone)]
pub struct CharPtr(*const libc::c_char);
impl CharPtr{
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub const fn null()->Self {
        CharPtr(null_mut())
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn from_str(s: &str)->Self {
        CharPtr(s.as_ptr() as *const libc::c_char)
    }
}
impl BoundedFormat for CharPtr {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        // Append all the bytes pointed to by the ptr untill we reach a null-byte
        let mut c = self.0;
        while unsafe{*c} != 0 {
            formatter.append(unsafe{*c as u8});
            c = unsafe{c.offset(1)};
        }
    }
}

type UIntPtr = libc::uintptr_t;
type IntPtr = libc::intptr_t;
type IFast32 = i64;
type UFast32 = u64;
type UFast64 = u64;
type AtomicUFast64 = AtomicU64; // std::atomic<uint_fast64_t>
type AtomicUFast32 = AtomicU64; // std::atomic<uint_fast32_t>
type AtomicIFast32 = AtomicI64; // std::atomic<int_fast32_t>
type Int = libc::c_int;
type UInt = libc::c_uint;
type AtomicUIntPtr = std::sync::atomic::AtomicUsize;

macro_rules! dflag {
	($n:ident = $v:expr) => [ $v ];
}

macro_rules! static_assert {
    ($condition:expr, $($arg:tt)*) => [assert!($condition, $($arg)*)];
}

// For Things that are 64-bits wide
pub(crate) trait Size64: PartialEq + BoundedFormat + Copy // A type with the same size as a ptr
{
    fn to_u64(self) -> u64;
    fn from_u64(val: u64) -> Self;
}
// Note: transmutes are used so that this will fail to compile
// if the types aren't the same size (wheras using 'as' will just truncate/zero-extend)

macro_rules! size_64_impl
{
    () => [
        #[inline(always)]
        fn to_u64(self) -> u64 { unsafe{transmute(self)} }
        #[inline(always)]
        fn from_u64(val: u64) -> Self { unsafe{transmute(val)} }
    ];
}
impl<T: Sized> Size64 for *mut T { size_64_impl!{} }
impl<T: Sized> Size64 for *const T { size_64_impl!{} }

// A type that should never be null
trait NonNull {
    fn not_null(self)->bool;
}
macro_rules! non_null_impl
{
    () => [
        #[inline(always)]
        fn not_null(self) -> bool {
            let v: usize = unsafe{transmute(self)};
            v != 0
        }
    ];
}


impl<T> NonNull for Shared<T> { non_null_impl!{} }
impl<'a, T> NonNull for &'a mut T { non_null_impl!{} }
impl<'a, T> NonNull for &'a T { non_null_impl!{} }

#[macro_use] mod _bounded_formatter; use self::_bounded_formatter::*;
#[macro_use] mod log; use self::log::*;
#[cfg(not(feature = "UNSAFE_OPT"))]
macro_rules! unwrap {
    ($e:expr) => [ $e._scalloc_unwrap().unwrap_or_else(|| fatal!("failed to unwrap Option")) ];
}
#[cfg(feature = "UNSAFE_OPT")]
macro_rules! unwrap {
    ($e:expr) => [ $e._scalloc_unwrap() ];
}

#[macro_use] mod _macros;
#[macro_use] mod _address_macros; use self::_address_macros::*;
mod _address; use self::_address::*;
mod _opt_address; use self::_opt_address::*;

#[macro_use] mod size_classes_raw; use self::size_classes_raw::*;
#[macro_use] mod platform; use self::platform::*;
mod globals; use self::globals::*;

mod utils; use self::utils::*;
mod arena; use self::arena::*;
mod atomic_value; use self::atomic_value::*;
mod core_id; use self::core_id::*;
mod lock; use self::lock::*;
mod deque; use self::deque::*;
mod large_objects; use self::large_objects::*;
mod size_classes; use self::size_classes::*;
mod free_list; use self::free_list::*;
mod stack; use self::stack::*;
mod span_pool; use self::span_pool::*;
mod span; use self::span::*;
mod core; use self::core::*;
mod lab; use self::lab::*;
mod glue; use self::glue::*;

#[no_mangle]
pub extern "C" fn scalloc_malloc(size: usize)->*mut Void {
    if cfg!(feature = "SCALLOC_NO_SAFE_GLOBAL_CONSTRUCTION") {
        if unlikely(unsafe{SCALLOC_GUARD_REF_COUNT == 0}) {
            unsafe{SCALLOC_GUARD_REF_COUNT += 1}
            scalloc_init();
        }
    }
    glue::malloc(size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_free(p: *mut Void) {
    glue::free(OptAddress::from(p))
}

#[no_mangle]
pub extern "C" fn scalloc_calloc(nmemb: usize, size: usize)->*mut Void {
    glue::calloc(nmemb, size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_realloc(ptr: *mut Void, size: usize)->*mut Void {
    glue::realloc(OptAddress::from(ptr), size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_memalign(__alignment: usize, __size: usize)->*mut Void {
    glue::memalign(__alignment, __size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_aligned_alloc(alignment: usize, size: usize)->*mut Void {
    glue::aligned_alloc(alignment, size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_posix_memalign(ptr: *mut *mut Void, align: usize, size: usize)->Int {
    glue::posix_memalign(unsafe{transmute(ptr)}, align, size)
}

#[no_mangle]
pub extern "C" fn scalloc_valloc(__size: usize)->*mut Void {
    glue::valloc(__size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_pvalloc(__size: usize)->*mut Void {
    glue::pvalloc(__size).into()
}

#[no_mangle]
pub extern "C" fn scalloc_malloc_stats() {
    glue::malloc_stats()
}

#[no_mangle]
pub extern "C" fn scalloc_mallopt(cmd: Int, value: Int)->Int {
    glue::mallopt(cmd, value)
}


#[no_mangle]
pub extern "C" fn scalloc_thread_start(arg: *mut Void)->*mut Void {
    unsafe{AB_SCHEDULER.get_me_alab()};
    let fake_args = *unsafe{Address::from(arg).to_ref::<ScallocStartArgs>()};
    (fake_args.real_start)(fake_args.real_args)
}

pub use self::platform::override_::*;



// This little trick is neccesary, as there is no way to get the type of an expression in Rust
#[inline(always)]
// Casts 'src' to the same type as '_dest' (useful for function overloading).
pub unsafe fn cast_ptr<Y, T>(src: *mut Y, _dest: *mut T)->*mut T { src as *mut T }

#[cfg(not(feature = "UNSAFE_OPT"))]
mod unwrapable
{
    pub trait Unwrapable<T> {
        // Like an 'unwrap' function, except that it calls 'fatal!' if it can't unwrap
        // as upposed to panic! (as the latter will do dynamic memory allocation).
        fn _scalloc_unwrap(self)->Option<T>;
    }
    use super::*;
    impl<T> Unwrapable<T> for Option<T> {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn _scalloc_unwrap(self)->Option<T> { self }
    }

    impl Unwrapable<Address> for OptAddress {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn _scalloc_unwrap(self)->Option<Address> { self.try_unwrap() }
    }
}

#[cfg(feature = "UNSAFE_OPT")]
mod unwrapable
{
    pub trait Unwrapable<T> {
        // Like an 'unwrap' function, except that it calls 'fatal!' if it can't unwrap
        // as upposed to panic! (as the latter will do dynamic memory allocation).
        fn _scalloc_unwrap(self)->T;
    }

    use super::*;
    macro_rules! unwrap_impl {
        ([$($gen:tt)*] $s:ty: $t:ty) => [
            impl<$($gen)*> Unwrapable<$t> for $s {
                #[inline(always)]
                fn _scalloc_unwrap(self)->$t {
                    unsafe{transmute(self)}
                }
            }
        ];
    }

    unwrap_impl!(['a, T] Option<&'a mut T>: &'a mut T);
    unwrap_impl!(['a, T] Option<&'a T>: &'a T);
    unwrap_impl!([] Option<Address>: Address);
    unwrap_impl!([T] Option<Shared<T>>: Shared<T>);
    unwrap_impl!([A0, A1, A2, A3, R] Option<extern "C" fn(A0, A1, A2, A3) -> R>: extern "C" fn(A0, A1, A2, A3) -> R);
    unwrap_impl!([A0] Option<unsafe extern "C" fn(A0)>: unsafe extern "C" fn(A0));
    unwrap_impl!([] OptAddress: Address);
}
pub use self::unwrapable::Unwrapable;

trait Indexable<T> {
    fn ref_index(&self, index: usize)->&T;
    fn mut_index(&mut self, index: usize)->&mut T;
}

trait ValIndexable<T>: Indexable<T> {
    fn val_index(&self, index: usize)->T;
}

#[cfg(not(feature = "UNSAFE_OPT"))]
mod indexable
{
    use super::*;
    macro_rules! val_indexable_impl {
        ([$($gen:tt)*] $s:ty: $t:ty) => [
            impl<$($gen)*> ValIndexable<$t> for $s {
                #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
                #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
                fn val_index(&self, index: usize)->$t {
                    self[index]
                }
            }
        ];
    }
    macro_rules! indexable_impl {
        ([$($gen:tt)*] $s:ty: $t:ty) => [
            impl<$($gen)*> Indexable<$t> for $s {
                #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
                #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
                fn ref_index(&self, index: usize)->&$t {
                    &self[index]
                }
                #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
                #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
                fn mut_index(&mut self, index: usize)->&mut $t {
                    &mut self[index]
                }
            }
        ];
    }

    macro_rules! array_impls {
        ($($size:tt)*) => [
            $(val_indexable_impl!([T: Copy] [T; $size]: T); indexable_impl!([T] [T; $size]: T);)*
        ];
    }
    array_impls!(16 20 29 128 516);
    val_indexable_impl!([T: Copy] [T]: T);
    indexable_impl!([T] [T]: T);
}

#[cfg(feature = "UNSAFE_OPT")]
mod indexable
{
    use super::*;
    macro_rules! val_indexable_impl {
        ([$($gen:tt)*] $s:ty: $t:ty) => [
            impl<$($gen)*> ValIndexable<$t> for $s {
                #[inline(always)]
                fn val_index(&self, index: usize)->$t {
                    unsafe{*self.get_unchecked(index)}
                }
            }
        ];
    }
    macro_rules! indexable_impl {
        ([$($gen:tt)*] $s:ty: $t:ty) => [
            impl<$($gen)*> Indexable<$t> for $s {
                #[inline(always)]
                fn ref_index(&self, index: usize)->&$t {
                    unsafe{self.get_unchecked(index)}
                }
                #[inline(always)]
                fn mut_index(&mut self, index: usize)->&mut $t {
                    unsafe{self.get_unchecked_mut(index)}
                }
            }
        ];
    }

    macro_rules! array_impls {
        ($($size:tt)*) => [
            $(val_indexable_impl!([T: Copy] [T; $size]: T); indexable_impl!([T] [T; $size]: T);)*
        ];
    }
    array_impls!(16 20 29 128 516);
    val_indexable_impl!([T: Copy] [T]: T);
    indexable_impl!([T] [T]: T);
}


#[cfg(feature = "RUST_PANIC_HACK")]
#[thread_local]
static mut SCALLOC_PANICKING: bool = false; // Is scalloc pannickinG?

// TODO: Mac stuff
pub use self::tls_base::AB;