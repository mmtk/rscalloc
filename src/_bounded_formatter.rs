// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

const LOG_LEN: usize = 512;

#[thread_local]
static mut FORMAT_BUFFER: [u8; LOG_LEN + 4] = [0; LOG_LEN + 4];

pub struct BoundedFormatter {
    pos: usize
}

impl BoundedFormatter {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn new()->BoundedFormatter {
        let buffer = unsafe{&mut FORMAT_BUFFER};
        let res = BoundedFormatter {
            pos: 0,
        };
        for i in 0..LOG_LEN {
            *buffer.mut_index(i) = 0xA; // 0xA = '\n'
        }
        // Add a '...\n' at the end, to be printed if the text dosn't fit in the buffer
        *buffer.mut_index(LOG_LEN+0) = 0x2E; // 0x2E = '.'
        *buffer.mut_index(LOG_LEN+1) = 0x2E;
        *buffer.mut_index(LOG_LEN+2) = 0x2E;
        *buffer.mut_index(LOG_LEN+3) = 0xA;

        res
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn append(&mut self, c: u8) {
        if self.pos < LOG_LEN {
            unsafe{*FORMAT_BUFFER.mut_index(self.pos) = c};
            self.pos += 1;
        } else {
            self.pos = LOG_LEN + 3 // so it will end with "...\n"
        }
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn to_str(&self)->&'static str {
        str::from_utf8(self.as_bytes()).unwrap()
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn as_bytes(&self)->&'static [u8] {
        unsafe{slice::from_raw_parts(FORMAT_BUFFER.ref_index(0), self.pos + 1)}
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn format<T: BoundedFormat>(&mut self, val: T) {
        val.format(self)
    }
}

pub trait BoundedFormat {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter);
}
impl<'a, T: Copy + BoundedFormat> BoundedFormat for &'a T {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        formatter.format(*self)
    }
}
impl<'a> BoundedFormat for &'a str {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        for c in self.as_bytes() {
            formatter.append(*c)
        }
    }
}

impl BoundedFormat for Utf8Error {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        formatter.format(self.description())
    }
}

impl<T> BoundedFormat for *const T {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        formatter.format(OptAddress::from(self))
    }
}
impl<T> BoundedFormat for *mut T {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn format(self, formatter: &mut BoundedFormatter) {
        formatter.format(OptAddress::from(self))
    }
}

macro_rules! bounded_format {
    ($($e:expr),*) => [{
        let mut f = BoundedFormatter::new();
        $(BoundedFormat::format($e, &mut f);)*
        f
    }]
}

macro_rules! bounded_format_impl {
    (Decimal: $S:ty, $U:ty) => [
        impl BoundedFormat for $S {
            #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
            #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
            fn format(self, formatter: &mut BoundedFormatter) {
                let i = if self < 0 {
                    formatter.append(0x2D); // 0x2D = '-'
                    -self as $U
                } else {
                    self as $ U
                };
                formatter.format(i)
            }
        }

        impl BoundedFormat for $U {
            #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
            #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
            fn format(mut self, formatter: &mut BoundedFormatter) {
                let mut s = [0u8; 20]; // Big enough for even a 64-bit integer
                let mut ss = 0;
                loop {
                    let remainder = (self % 10) as u8;
                    self = self / 10;
                    *s.mut_index(ss) = 0x30 + remainder; // 0x30 = '0'
                    ss += 1;
                    if self == 0 {
                        break;
                    }
                }
                for i in (0..ss).rev() {
                    formatter.append(s.val_index(i));
                }
            }
        }
    ];
    (Hexadecimal: $U:ty) => [
        impl BoundedFormat for $U {
            #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
            #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
            fn format(self, formatter: &mut BoundedFormatter) {
                // append '0x'
                formatter.append(0x30);
                formatter.append(0x78);

                let mut i: usize = self.into();
                let mut s = [0u8; 16]; // Big enough for even a 64-bit integer
                let mut ss = 0;
                loop {
                    let remainder = (i % 16) as u8;
                    i = i / 16;
                    if remainder < 10 {
                        *s.mut_index(ss) = 0x30 + remainder; // 0x30 == '0'
                    } else {
                        *s.mut_index(ss) = 0x41 + (remainder - 10); // 0x41 == 'A'
                    }
                    ss += 1;
                    if i == 0 {
                        break;
                    }
                }
                for i in (0..ss).rev() {
                    formatter.append(s.val_index(i));
                }
            }
        }
    ];
}

bounded_format_impl!(Decimal: i8, u8);
bounded_format_impl!(Decimal: i16, u16);
bounded_format_impl!(Decimal: i32, u32);
bounded_format_impl!(Decimal: i64, u64);
bounded_format_impl!(Decimal: isize, usize);
bounded_format_impl!(Hexadecimal: OptAddress);
bounded_format_impl!(Hexadecimal: Address);