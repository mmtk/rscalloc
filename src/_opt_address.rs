// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;
// Handles pointer operations for us
#[derive(Clone, Copy)]
#[repr(C)]
pub struct OptAddress(*mut Void);
impl OptAddress {
    #[inline(always)]
    pub fn try_unwrap(self)->Option<Address> { Address::try_new(self.to_void_ptr()) }

    #[inline(always)]
    pub fn unwrap_or(self, def: Address)->Address { self.try_unwrap().unwrap_or(def) }

    #[inline(always)]
    pub fn unwrap_or_else<F: FnOnce()->Address>(self, f: F)->Address { self.try_unwrap().unwrap_or_else(f) }

    #[inline(always)]
    pub const fn null() -> OptAddress { OptAddress(null_mut()) }

    #[inline(always)]
    pub fn is_null(self) -> bool { self.0.is_null() }

    #[inline(always)]
    fn new<T>(value: *const T) -> OptAddress { OptAddress(value as *mut Void) }

    #[inline(always)]
    pub fn to_ptr<T>(self) -> *mut T { self.0 as *mut T }

    address_methods!();
}
impl Size64 for OptAddress { size_64_impl!{} }

address_impl!(OptAddress);

impl From<Address> for OptAddress {
    #[inline(always)]
    fn from(val: Address)->OptAddress { OptAddress::new(val.to_void_ptr()) }
}