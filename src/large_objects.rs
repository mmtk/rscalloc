// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub const MAGIC: u64 = 0xAAAAAAAAAAAAAAAA;
#[repr(C)]
pub struct LargeObject {
    actual_size: usize,
    magic: u64,
}
// pub alocate
//pub free
//pub PayloadSize
impl LargeObject {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn payload_size(&self) -> usize { self.actual_size - size_of_val(self) }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn actual_size(&self) -> usize { self.actual_size }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn validate(&self) -> bool { self.magic == MAGIC }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn from_mutator_ptr(p: Address) -> Shared<LargeObject> {
        let mut obj: Shared<LargeObject> = (p & (PAGE_NR_MASK as usize)).into();
        if !unsafe{obj.as_mut()}.validate() {
            fatal!("invalid large object: ", p);
        }
        obj
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn allocate(size: usize) -> Address {
        let actual_size = pad_size(size + size_of::<LargeObject>(), PAGE_SIZE);
        let obj = unsafe{system_mmap_fail(actual_size).to_mut::<LargeObject>()};
        *obj = LargeObject::new(actual_size);

        let obj = if cfg!(feature = "SCALLOC_DEBUG") {
            //: Force the check by going through the mutator pointer.
            unsafe{&mut *LargeObject::from_mutator_ptr(obj.object_start()).as_ptr()}
        } else {
            obj
        };

        Address::from(obj.object_start())
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn free(p: Address) {
        let obj = Address::from(LargeObject::from_mutator_ptr(p));
        if unsafe{libc::munmap(obj.into(), obj.to_mut::<LargeObject>().actual_size())} != 0 {
            fatal!("munmap failed")
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn payload_size_ptr(p: Address) -> usize {
        unsafe{&mut*LargeObject::from_mutator_ptr(p).as_ptr()}.payload_size()
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn new(size: usize) -> Self {
        Self {
            actual_size: size,
            magic: MAGIC,
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn object_start(&mut self) -> Address {
        Address::from(self) + size_of::<Self>()
    }
}
