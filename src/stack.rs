// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub mod stack {
    use super::*;
    type TopPtr = TaggedValue<OptAddress>;


    pub(crate) trait Stack {
        #[inline(always)]
        fn _top_mut(&mut self) -> &mut AtomicTaggedValue<OptAddress>;
        #[inline(always)]
        fn _top_ref(&self) -> &AtomicTaggedValue<OptAddress>;

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn empty(&self) -> bool { self._top_ref().load() == TopPtr::new(OptAddress::null(), 0) }

        /*TODO: Unused
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn set_top(&mut self, p: OptAddress) { self._top_mut().store(&TopPtr::new(p, 0)) }*/

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn push_range(&mut self, p_start: OptAddress, p_end: Address)->i32 {
            let mut top_old: TopPtr;
            do_!{{
                top_old = self._top_ref().load();
                unsafe{*p_end.to_mut() = top_old.value()};
            } while !self._top_mut().swap(&top_old, &TopPtr::new(p_start, top_old.tag() + 1))};
            (top_old.tag() + 1) as i32
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn push(&mut self, p: Address) -> i32 {
            trace!("push ", p);
            self.push_range(p.into(), p)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn pop(&mut self) -> OptAddress {
            let mut top_old: TopPtr;
            do_!{{
                top_old = self._top_ref().load();
                if top_old.value().is_null() {
                    break;
                };

            // Why is this +1 and not -1??? It makes no sense...
            // (as the tag is used to store the length, right??)
            } while !self._top_mut().swap(&top_old,
                &TopPtr::new(*unsafe{top_old.value().to_ref()}, top_old.tag() + 1))};

            trace!("pop ", top_old.value());
            top_old.value()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn pop_all(&mut self) -> (OptAddress, i32) {
            let mut top_old: TopPtr;
            do_!{{
                top_old = self._top_ref().load();
                if top_old.value().is_null() {
                    break;
                };
            } while !self._top_mut().swap(&top_old, &TopPtr::new(OptAddress::null(), 0))};
            (
                /* elements */ top_old.value(),
                /* len */ top_old.tag() as i32,
            )
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn length(&self) -> IFast32 {
            self._top_ref().load().tag() as IFast32
        }
    }

    #[repr(C)]
    pub struct Stack64 {
        top: AtomicTaggedValue<OptAddress>,
        __pad: [i8; 64 - size_of::<AtomicTaggedValue<OptAddress>>()%64],
    }
    impl Stack64 {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn new() -> Self {
            Self {
                top: AtomicTaggedValue::default(),

                __pad: [0; 64 - size_of::<AtomicTaggedValue<OptAddress>>()%64],
                //unsafe { uninitialized() }, // SAFE: it's just padding
            }
        }
    }
    impl Stack for Stack64 {
        #[inline(always)]
        fn _top_mut(&mut self) -> &mut AtomicTaggedValue<OptAddress> {
            &mut self.top
        }
        #[inline(always)]
        fn _top_ref(&self) -> &AtomicTaggedValue<OptAddress> {
            &self.top
        }
    }

    #[repr(C)]
    pub struct Stack128 {
        top: AtomicTaggedValue<OptAddress>,
        __pad: [i8; 128 - size_of::<AtomicTaggedValue<OptAddress>>()%128],
    }
    impl Stack128 {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn new() -> Self {
            Self {
                top: AtomicTaggedValue::default(),

                __pad: [0; 128 - size_of::<AtomicTaggedValue<OptAddress>>()%128],
                //unsafe { uninitialized() }, // SAFE: it's just padding
            }
        }
    }
    impl Stack for Stack128 {
        #[inline(always)]
        fn _top_mut(&mut self) -> &mut AtomicTaggedValue<OptAddress> { &mut self.top }
        #[inline(always)]
        fn _top_ref(&self) -> &AtomicTaggedValue<OptAddress> { &self.top }
    }
}

// Rustc is being stupid, 'Stack' itself is not used but it's functions are
// so rustc tells me the import is unnecesary, removing it however causes errors
pub(crate) use self::stack::{Stack, Stack64, Stack128};
