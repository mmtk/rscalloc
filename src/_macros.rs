// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
// Macros for missing Rust features...
macro_rules! source_info {
    () => [(_filename(file!()), line!())];
}

macro_rules! do_
{
    ({$($b:stmt;)*} while $c:expr) => [loop{$($b;)* if !$c {break;}}];
}

// Please god don't use this...
/*macro_rules! for_
{
    ($i:stmt; $c:expr; $n:stmt; $b:block) => [{$i; while $c { $b; $n }}];
}*/

macro_rules! aligned
{
    ($V:vis type $n:ident ($($a:ty),*)) => [
        #[repr(simd)]
        #[derive(Copy, Clone)]
        struct Align($($a),*);

        #[repr(C)]
        $V struct $n<T> { val: T, __align: [Align; 0] }
        // SAFE: as Sync for T is implemented
        unsafe impl<T: Sync> Sync for $n<T> { }
        impl<T> $n<T> {
            #[inline(always)]
            pub const fn new(value: T)->$n<T> {
                Self { val: value, __align: [] }
            }
        }
        impl<T> std::ops::Deref for $n<T> {
            type Target = T;
            #[inline(always)]
            fn deref(&self) -> &T { &self.val }
        }
        impl<T> std::ops::DerefMut for $n<T> {
            #[inline(always)]
            fn deref_mut(&mut self) -> &mut T { &mut self.val }
        }
        /*impl<T> std::convert::Into<T> for $n<T> {
            #[inline(always)]
            fn into(self) -> T { self.val }
        }*/
        impl<T> std::convert::From<T> for $n<T> {
            #[inline(always)]
            fn from(val: T) -> Self { Self::new(val) }
        }

        impl<T> std::convert::AsRef<T> for $n<T> {
            #[inline(always)]
            fn as_ref(&self) -> &T { &self.val }
        }
        impl<T> std::convert::AsMut<T> for $n<T> {
            #[inline(always)]
            fn as_mut(&mut self) -> &mut T { &mut self.val }
        }
    ];

    (($($a:ty),*) $(#[$atr:meta])* $V:vis static mut $n:ident: $t:ty = $val:expr) => [
        aligned!($V type Aligned ($($a),*));
        $(#[$atr])*
        $V static mut $n: Aligned<$t> = Aligned::new($val);
    ];
    (($($a:ty),*) $(#[$atr:meta])* $V:vis static $n:ident: $t:ty = $val:expr) => [
        aligned!($V type Aligned ($($a),*));
        $(#[$atr])*
        $V static $n: Aligned<$t> = Aligned::new($val);
    ];
}

macro_rules! zeroed {
    ($V:vis type $R:ident = $T:ty) => [
        #[repr(C)]
        $V struct $R {
            pub(self) data: [u8; std::mem::size_of::<$T>()],
            pub(self) align: [$T; 0]
        }
        impl $R {
            pub const fn zeroed()->Self {
                Self {
                    data: [0; std::mem::size_of::<$T>()],
                    align: []
                }
            }
        }
        impl std::ops::Deref for $R {
            type Target = $T;
            #[inline(always)]
            fn deref(&self) -> &$T { unsafe{std::mem::transmute(self)} }
        }
        impl std::ops::DerefMut for $R {
            #[inline(always)]
            fn deref_mut(&mut self) -> &mut $T { unsafe{std::mem::transmute(self)} }
        }
        impl std::convert::From<$T> for $R {
            #[inline(always)]
            fn from(val: $T)->Self { unsafe{std::mem::transmute(val)} }
        }
        impl std::convert::AsRef<$T> for $R {
            #[inline(always)]
            fn as_ref(&self)->&$T { unsafe{std::mem::transmute(self)} }
        }
        impl std::convert::AsMut<$T> for $R {
            #[inline(always)]
            fn as_mut(&mut self)->&mut $T { unsafe{std::mem::transmute(self)} }
        }
    ];

    ($V:vis static $N:ident: $T:ty) => [
        zeroed!($V type Z = $T);
        $V static $N: Z = Z::zeroed();
    ];

    ($V:vis static mut $N:ident: $T:ty) => [
        zeroed!($V type Z = $T);
        $V static mut $N: Z = Z::zeroed();
    ];
}

// For accesing a quadword tls
macro_rules! tls64 {
    // Gets the address
    (&$TL:ident) => [{
        let res: *mut Void;
        // Uses the initial exec model
        asm!(concat!("
            MOV $0, FS:0
            ADD $0, [RIP + ", stringify!($TL), "@GOTTPOFF]") : "=r"(res) ::: "intel"
        );
        cast_ptr(res, &mut $TL)
    }];

    // Does a load (with a cast)
    ($TL:ident as $t:ty) => [{
        let res: u64;

        // Uses the initial exec model
        asm!(concat!("
            MOV $0, [RIP + ", stringify!($TL), "@GOTTPOFF]
            MOV $0, FS:[$0]") : "=r"(res) ::: "intel"
        );
        <$t as Size64>::from_u64(res)
    }];
    // Does a store
    ($TL:ident = $val:expr) => [{
        let val = $val.to_u64();
        let _offset: u64;

        asm!(concat!("
            MOV $0, [RIP + ", stringify!($TL), "@GOTTPOFF]
            MOV FS:[$0], $1") : "=&r"(_offset) : "r"(val) :: "volatile", "intel"
        );
    }];
}
