// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[inline(always)]
//: __FILE__ expands to the full path. Strip basename for __FILENAME__.
pub fn _filename(file: &str)->&str {
    file.split_at(file.rfind('/').map(|x| x + 1).unwrap_or_else(
        || file.rfind('\\').map_or(0, |x| x + 1))).1
}

macro_rules! _log_format
{
    ($($arg:expr),*) => [
        //: Start with "__FILENAME__:__LINE__ ".
        bounded_format!(_filename(file!()), ":", line!(), " ", $($arg),*)
    ];
}

// Note: if this fails to print, the program will abort
pub fn err_print(msg: BoundedFormatter) {
    let msg = msg.as_bytes();

    // Call posix IO functions directly (as Rust's functions do allocation)
    // (Don't worry, this function is allegedly threadsafe)
    if unsafe{libc::write(libc::STDERR_FILENO, msg.as_ptr() as *const Void, msg.len())} as usize
            != msg.len() {
    //    abort(); // write failed, give up
    }

    unsafe{libc::fsync(libc::STDERR_FILENO)};
}
macro_rules! trace {
    ($($arg:expr),*) => [{if cfg!(feature = "SCALLOC_TRACE") {err_print(_log_format!($($arg),*));}}];
}
macro_rules! warning {
    ($($arg:expr),*) => [{if cfg!(feature = "SCALLOC_WARNING") {err_print(_log_format!($($arg),*));}}];
}

pub fn panic(msg: BoundedFormatter)->! {
    if cfg!(feature = "RUST_PANIC_HACK") || cfg!(not(feature = "OVERRIDE_MALLOC")) {
        #[cfg(feature = "RUST_PANIC_HACK")] {
            unsafe{SCALLOC_PANICKING = true};
        }
        panic!(msg.to_str());
    } else {
        err_print(msg);
        abort()
    }
}

macro_rules! fatal
{
	($($arg:expr),*) => [{
        panic(_log_format!($($arg),*))
    }];
}
