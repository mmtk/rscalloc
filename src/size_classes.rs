// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))] // __attribute__((pure))
pub fn size_to_class(size: usize) -> i32 {
    if size <= MAX_SMALL_SIZE {
        ((size + MIN_ALIGNMENT - 1) / MIN_ALIGNMENT) as i32
    } else if size <= MAX_MEDIUM_SIZE {
        (log2(size - 1) as usize - MAX_SMALL_SHIFT + FINE_CLASSES) as i32
    } else {
        0 // 0 indicates size of 0 or large objects.
    }
}

/*TODO: Unused
#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn size_to_block_size(size: usize) -> i32 {
    if size <= MAX_SMALL_SIZE {
        ((size + MIN_ALIGNMENT - 1) & !(MIN_ALIGNMENT - 1)) as i32
    } else if size <= MAX_MEDIUM_SIZE {
        1 << log2(size - 1) + 1
    } else {
        unreachable!();
        // 0
    }
}*/


/*TODO: Unused
#[inline]
pub fn print_size_classes() {
    eprintln!("Sizeclass summary");
    eprintln!("Reuse theshold: {}%", REUSE_THRESHOLD);
    let mut waste: i32 = 0;
    for i in 0..NUM_CLASSES {
        if i > 0 {
            waste = CLASS_TO_SIZE.val_index(i as usize) - CLASS_TO_SIZE.val_index((i - 1) as usize) - 1;
            waste = (waste * 100) / CLASS_TO_SIZE.val_index(i as usize);
        }
        eprintln!(
            "[9] \t.val_index({}) \
             size: {}, \
             objects: {}, \
             realspan size: {}, \
             reuse theshold: {}, \
             waste: {}%",
            i,
            CLASS_TO_SIZE.val_index(i as usize),
            CLASS_TO_OBJECTS.val_index(i as usize),
            CLASS_TO_SPAN_SIZE.val_index(i as usize),
            CLASS_TO_REUSE_THRESHOLD.val_index(i as usize),
            waste
        );
    }
    abort();
}*/