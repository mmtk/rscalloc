// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[repr(C)]
pub struct IncrementalFreeList {
    list: OptAddress,
    bump_pointer: IntPtr,
    len: i32,
    increment: i32,
}
impl IncrementalFreeList {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn length(&self) -> IFast32 {
        self.len as IFast32
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn new(start: IntPtr, size_class: usize) -> Self {
        Self {
            list: OptAddress::null(),
            bump_pointer: start,
            len: CLASS_TO_OBJECTS.val_index(size_class),
            increment: CLASS_TO_SIZE.val_index(size_class),
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn push(&mut self, obj: Address) -> i32 {
        unsafe{*obj.to_mut() = self.list}
        self.list = obj.into();
        self.len += 1;
        self.len
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn set_list(&mut self, objs: OptAddress, len: usize) {
        self.list = objs;
        self.len = len as i32;
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn pop(&mut self) -> OptAddress {
        let result = self.list;
        if !result.is_null() {
            self.list = unsafe{*self.list.to_ref()};
            self.len -= 1;
            result
        } else {
            if unlikely(self.len == 0) {
                OptAddress::null()
            } else {
                let result = OptAddress::from(self.bump_pointer);
                self.bump_pointer += self.increment as IntPtr;
                self.len -= 1;
                result
            }
        }
    }
}
