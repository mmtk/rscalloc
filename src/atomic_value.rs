// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;
// use platform::globals::*;

pub mod tagged_value {
    use super::*;

    pub type RawType = u64;
    pub type TagType = u16;
    pub type ValueType<T> = T;

    //pub const MAX_TAG: TagType = ((1u64 << 49) - 1) as TagType;
    const VALUE_BITS: RawType = 48;
    const VALUE_MASK: RawType = (1 << VALUE_BITS) - 1;
    const EXTEND_MASK: RawType = std::u64::MAX - VALUE_MASK;

    #[derive(Eq, Debug)]
    #[repr(C)]
    pub(crate) struct TaggedValue<T: Size64> {
        pub(super) raw: RawType,
        __marker: PhantomData<ValueType<T>>,
    }
    impl<T: Size64> TaggedValue<T> {
        /*TODO: Unused
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn check_compatibility(value: T) {
            if cfg!(feature = "TAGGED_VALUE_CHECKED_MODE") {
                assert_eq!(T::from_u64(value.to_u64() & VALUE_MASK), value);
            }
        }*/

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub const fn default() -> Self {
            Self {
                raw: 0,
                __marker: PhantomData::<T>{},
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new(value: T, tag: TagType) -> Self {
            let this = Self {
                raw: (value.to_u64() & VALUE_MASK) | ((tag as RawType) << 48),
                __marker: PhantomData::<T> {},
            };

            if cfg!(feature = "TAGGED_VALUE_CHECKED_MODE") {
                if (this.value() != value) || (this.tag() != tag) {
                    //was fprintf(stderr, ...) + abort()
                    fatal!("TaggedValue inconsistency: tried creating with \
                         value: ", Address::from(value.to_u64() as UIntPtr), ", tag: ", tag,
                         ". result: value(): ", Address::from(this.value().to_u64() as UIntPtr),
                         ", tag(): ", this.tag(), ".");
                }
            }
            static_assert!(
                size_of::<T>() <= 8, /* only approximate check */
                "tagging requires the value to have less than 48 bits, \
                 or 48 bits with the 48'th bit being extended"
            );
            this
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn value(&self) -> T {
            T::from_u64((self.raw & VALUE_MASK) | (((self.raw >> (VALUE_BITS - 1))&0x1)*EXTEND_MASK))
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn tag(&self) -> TagType {
            (self.raw >> VALUE_BITS) as TagType
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn new_raw(raw: RawType) -> Self {
            Self {
                // Tags start from the upper 48-bits downards
                raw: raw,
                __marker: PhantomData::<ValueType<T>> {},
            }
        }
    }

    impl<T: Size64> Copy for TaggedValue<T> {}
    impl<T: Size64> Clone for TaggedValue<T> {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn clone(&self) -> Self {
            //: See operator=.
            //: No copy-and-swap since we only need to manage primitive members.
            *self
        }
    }
    impl<T: Size64> PartialEq for TaggedValue<T> {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn eq(&self, other: &Self) -> bool {
            self.raw == other.raw
        }
    }

    impl<T: Size64> BoundedFormat for TaggedValue<T> {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn format(self, formatter: &mut BoundedFormatter) {
            formatter.format(Address::from(self.raw as usize))
        }
    }
}
pub(crate) use self::tagged_value::TaggedValue;

#[repr(C)]
pub(crate) struct AtomicTaggedValue<T: Size64> {
    raw_atomic: AtomicU64, // Atomic<tagged_value::RawType>
    //__padding: [u8; if PAD != 0 {PAD - size_of::<AtomicU64>()} else { 0 }],
    __marker: PhantomData<tagged_value::ValueType<T>>,
}

impl<T: Size64> AtomicTaggedValue<T> {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub const fn default() -> Self {
        Self {
            raw_atomic: AtomicU64::new(0),
            //__padding: unsafe{uninitialized()},
            __marker: PhantomData::<tagged_value::ValueType<T>> {},
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn new(tagged_value: &TaggedValue<T>) -> Self {
        Self {
            raw_atomic: AtomicU64::new(tagged_value.raw),
            //__padding: unsafe{uninitialized()},
            __marker: PhantomData::<tagged_value::ValueType<T>> {},
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn load(&self) -> TaggedValue<T> {
        TaggedValue::<T>::new_raw(self.raw_atomic.load(Ordering::SeqCst))
    }

    /*TODO: Unused
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn store(&mut self, tagged_value: &TaggedValue<T>) {
        self.raw_atomic.store(tagged_value.raw, Ordering::SeqCst)
    }*/

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn swap(&mut self, expected: &TaggedValue<T>, desired: &TaggedValue<T>) -> bool {
        let val = expected.raw;
        self.raw_atomic.compare_and_swap(val, desired.raw, Ordering::SeqCst) == val
    }

    //always_inline void* operator new(size_t size) // only usefull when ALIGN != 0
    //always_inline void operator delete(void* ptr)  // only usefull when using the above new
}
// SAFE: its just an AtomicU64, which implements Sync
unsafe impl<T: Size64> Sync for AtomicTaggedValue<T> { }