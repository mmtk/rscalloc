// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

#[repr(C)]
pub struct DoubleListNode {
    // (next, prev)
    link: Option<(Shared<DoubleListNode>, Shared<DoubleListNode>)>,
}
impl DoubleListNode {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub const fn default()->Self {
        Self {
            link: None,
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn next(&mut self) -> Option<Shared<DoubleListNode>> { self.link.map(|x| x.0) }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn prev(&mut self) -> Option<Shared<DoubleListNode>> { self.link.map(|x| x.1) }

    pub(self) fn set(&mut self, prev: &mut DoubleListNode, next: &mut DoubleListNode)->(Shared<DoubleListNode>, Shared<DoubleListNode>) {
        scalloc_assert!(next.not_null());
        scalloc_assert!(prev.not_null());
        let res = (Shared::from(prev), Shared::from(next));
        self.link = Some((res.1, res.0)); // Reverse order for storage is reversed here
        res
    }

    pub(self) fn set_prev(&mut self, prev: &mut DoubleListNode)->Shared<DoubleListNode> {
        let next = unsafe{&mut *self.next().unwrap().as_ptr()};
        self.set(prev, next).0
    }
    pub(self) fn set_next(&mut self, next: &mut DoubleListNode)->Shared<DoubleListNode> {
        let prev = unsafe{&mut *self.prev().unwrap().as_ptr()};
        self.set(prev, next).1
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub(self) fn get_next<'a, 'b>(&'a mut self)->&'b mut DoubleListNode {
        unsafe{&mut *unwrap!(self.next()).as_ptr()}
    }
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub(self) fn get_prev<'a, 'b>(&'a mut self)->&'b mut DoubleListNode {
        unsafe{&mut *unwrap!(self.prev()).as_ptr()}
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub(self) fn clear(&mut self) {
        self.link = None;
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn is_empty(&self)->bool {
        self.link.is_none()
    }
}

use lock::spin_lock_0 as lock;
type Lock = SpinLock0;

//: A simple sequential double-ended queue (deque) allowing constant time insert
//: (front, back) and remove (front, back, and specific node).
#[repr(C)]
pub(crate) struct Deque {
    lock: Lock,
    owner: CoreId,
    sentinel: DoubleListNode,
    __pad: [Byte; 64 - (size_of::<Lock>() + size_of::<DoubleListNode>()) % 64],
}

impl Default for Deque{
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn default() -> Deque {
        Deque {
			lock: Lock::new(),
			owner: CoreId::default(),
			sentinel: DoubleListNode::default(),
			// SAFE: field is never used (it is literally just padding)
			__pad: unsafe { uninitialized() },
		}
    }
}
impl Deque {
	#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
	#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
	// We have to split this up from 'default', as it relies on the value of 'this.sentinel()'
	// which is relative to the location of self (so we only call this onc we've
	// placed the result of default in a fixed memory location)
	pub fn init(&mut self) {
		self.lock.unlock();
        let sentinel1 = self.sentinel();
        let sentinel2 = self.sentinel();
		self.sentinel.set(sentinel1, sentinel2);
	}

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn sentinel<'a, 'b>(&'a mut self)->&'b mut DoubleListNode {
        unsafe{&mut *((&mut self.sentinel) as *mut DoubleListNode)}
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn open(&mut self, owner: CoreId) {
        self.owner = owner;
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn close(&mut self) {
        self.owner = *TERMINATED;
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn push_front(&mut self, owner: CoreId, mut node: Shared<DoubleListNode>) {
        // The drop function for this has side-affects
        let _ = lock::Guard::new(&mut self.lock);

        scalloc_assert!(node.not_null());
        if owner == self.owner {
            let node = unsafe{node.as_mut()};

            node.set(self.sentinel(), self.sentinel.get_next());

            self.sentinel.get_next().set_prev(node); // I.e. net the next value is null()
            self.sentinel.set_next(node);
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn push_back(&mut self, owner: CoreId, mut node: Shared<DoubleListNode>) {
        // The drop function for this has side-affects
        let _ = lock::Guard::new(&mut self.lock);

        scalloc_assert!(node.not_null());
        if owner == self.owner {
            let node = unsafe{node.as_mut()};

            node.set(self.sentinel.get_prev(), self.sentinel());

            self.sentinel.get_prev().set_next(node);
            self.sentinel.set_prev(node);
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn remove_front(&mut self) -> Option<Shared<DoubleListNode>> {
        // The drop function for this has side-affects
        let _ = lock::Guard::new(&mut self.lock);

        let node_addr = Address::from(self.sentinel.get_next());
        let node = self.sentinel.get_next();

        self.sentinel.set_next(node.get_next());
        node.get_next().set_prev(self.sentinel());

        if node_addr == Address::from(self.sentinel()) {
            None
        } else {
            node.clear();
            Some(Shared::from(node))
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn remove_back(&mut self) -> Option<Shared<DoubleListNode>> {
        // The drop function for this has side-affects
        let _ = lock::Guard::new(&mut self.lock);

        let node_addr = Address::from(self.sentinel.get_prev());
        let node = self.sentinel.get_prev();

        self.sentinel.set_prev(node.get_prev());
        node.get_prev().set_next(self.sentinel());

        if node_addr == Address::from(self.sentinel()) {
            None
        } else {
            node.clear();
            Some(Shared::from(node))
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn remove(&mut self, owner: CoreId, mut node: Shared<DoubleListNode>) {
        // The drop function for this has side-affects
        let _ = lock::Guard::new(&mut self.lock);

        let node = unsafe{node.as_mut()};
        if !node.is_empty() && owner == self.owner {

            // if node.next().is_some()
            node.get_next().set_prev(node.get_prev());

            // if node.prev().is_some()
            node.get_prev().set_next(node.get_next());

            node.clear();
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn remove_all(&mut self) {
        //: No lock here! ;)

        while self.remove_front().is_some() {}
    }
}