// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
pub const SPAN_HEADER_SIZE: usize = 128;
pub const SIZE_CLASS_COUNT: usize = 29;

macro_rules! for_all_size_classes {
	($v:expr) => [
		[$v(0, 0, 0, 0),
		$v(1, 16, 32768, (32768 - SPAN_HEADER_SIZE)/16),
		$v(2, 32, 32768, (32768 - SPAN_HEADER_SIZE)/32),
		$v(3, 48, 32768, (32768 - SPAN_HEADER_SIZE)/48),
		$v(4, 64, 32768, (32768 - SPAN_HEADER_SIZE)/64),
		$v(5, 80, 32768, (32768 - SPAN_HEADER_SIZE)/80),
		$v(6, 96, 32768, (32768 - SPAN_HEADER_SIZE)/96),
		$v(7, 112, 32768, (32768 - SPAN_HEADER_SIZE)/112),
		$v(8, 128, 32768, (32768 - SPAN_HEADER_SIZE)/128),
		$v(9, 144, 32768, (32768 - SPAN_HEADER_SIZE)/144),
		$v(10, 160, 32768, (32768 - SPAN_HEADER_SIZE)/160),
		$v(11, 176, 32768, (32768 - SPAN_HEADER_SIZE)/176),
		$v(12, 192, 32768, (32768 - SPAN_HEADER_SIZE)/192),
		$v(13, 208, 32768, (32768 - SPAN_HEADER_SIZE)/208),
		$v(14, 224, 32768, (32768 - SPAN_HEADER_SIZE)/224),
		$v(15, 240, 32768, (32768 - SPAN_HEADER_SIZE)/240),
		$v(16, 256, 32768, (32768 - SPAN_HEADER_SIZE)/256),
		$v(17, 512, ((64 * 512 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 64),
		$v(18, 1024, ((64 * 1024 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 64),
		$v(19, 2048, ((64 * 2048 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 64),
		$v(20, 4096, ((32 * 4096 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 32),
		$v(21, 8192, ((32 * 8192 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 32),
		$v(22, 16384, ((16 * 16384 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 16),
		$v(23, 32768, ((16 * 32768 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 16),
		$v(24, 65536, ((16 * 65536 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 16),
		$v(25, 131072, ((8 * 131072 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 8),
		$v(26, 262144, ((4 * 262144 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 4),
		$v(27, 524288, ((2 * 524288 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 2),
		$v(28, 1048576, ((1 * 1048576 + SPAN_HEADER_SIZE)/PAGE_SIZE + 1) * PAGE_SIZE, 1)]
	];
}
