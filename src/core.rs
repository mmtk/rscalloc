// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

mod core {
    use super::*;
    pub(super) type RemoteFullSpans = Stack64;

    #[repr(C)]
    pub(crate) struct Core {
        pub(super) core_link: Address,
        pub(super) id: CoreId,
        pub(super) hot_span: [*mut Span; NUM_CLASSES as usize],
        pub(super) r_spans: [Deque; NUM_CLASSES as usize],
        pub(super) _pad: [u8;
            128
                - (size_of::<Address>() + size_of::<CoreId>()
                    + size_of::<[*mut Span; NUM_CLASSES as usize]>())
                    % 128],
    }

    impl Core {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn id(&self) -> CoreId {
            self.id
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new(&mut self) -> &mut Self {
            self.r_spans = Default::default();
            for deque in &mut self.r_spans {
                deque.init();
            }
            if cfg!(feature = "SCALLOC_DEBUG") {
                self.check_alignments();
            }
            self
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn check_alignments(&self) {
            //: Dynamically check field alignment for 32bit.

            fn check_field<T>(field: &T, name: &str) {
                if !(OptAddress::from(field) & 0x3).is_null() {
                    fatal!("Field ", name, " not properly aligned.");
                }
            }

            check_field(&self.core_link, "core_link");
            check_field(&self.id, "id");
            check_field(&self.hot_span, "hot_span");
            check_field(&self.r_spans, "r_spans");
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn init(&mut self, id: CoreId) {
            self.id = id;
            for i in 0..NUM_CLASSES {
                self.r_spans.mut_index(i as usize).open(id);
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        unsafe fn hot_span(&mut self, index: usize)->&mut Span {
            &mut *self.hot_span.val_index(index)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn destroy(&mut self) {
            for i in 0..NUM_CLASSES as usize {
                self.r_spans.mut_index(i).close();

                if !self.hot_span.val_index(i).is_null() {
                    unsafe{self.hot_span(i)}.new_mark_floating();
                    *self.hot_span.mut_index(i) = null_mut();
                }

                self.r_spans.mut_index(i).remove_all();
            }
            self.id = *TERMINATED;
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn get_span(&mut self, sc: i32) -> *mut Span {
            let mut new_span = null_mut::<Span>();
            while let Some(node) = self.r_spans.mut_index(sc as usize).remove_back() {
                let new_span_val = Span::from_span_link(node);
                let epoch = new_span_val.epoch();
                if new_span_val.new_mark_hot(epoch) {
                    scalloc_assert_eq!(new_span_val.owner(), self.id());
                    new_span_val.move_remote_to_local_objects();
                    new_span = new_span_val;
                    break;
                }
            }
            let new_span = if new_span.is_null() {
                Span::new(sc as usize, self.id())
            } else {
                new_span
            };

            if cfg!(feature = "SCALLOC_NO_CLEANUP_IN_FREE") {
                while let Some(node) = self.r_spans.mut_index(sc as usize).remove_back() {
                    let cleanup_span = Span::from_span_link(node);
                    let epoch = cleanup_span.epoch();
                    if cleanup_span.nr_free_objects() == CLASS_TO_OBJECTS.val_index(cleanup_span.size_class()) as IFast32 {
                        let sucess = cleanup_span.new_mark_full(epoch);
                        scalloc_assert!(sucess); //: should always work
                        Span::delete(cleanup_span);
                    }
                }
            }

            new_span
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn allocate(&mut self, size: usize) -> OptAddress {
            scalloc_assert_ne!(self.id(), *TERMINATED);
            let sc = size_to_class(size) as usize;
            if sc >= NUM_CLASSES as usize {
                fatal!("size class ", sc, " for size ", size, " does not exist");
            }
            if unlikely(self.hot_span.val_index(sc).is_null()) {
                if unlikely(sc == 0) {
                    //: Could either be allocation for size 0, or a really large object.
                    return if unlikely(size == 0) {
                        OptAddress::null()
                    } else {
                        LargeObject::allocate(size).into()
                    };
                } else {
                    *self.hot_span.mut_index(sc) = self.get_span(sc as i32);
                }
            }
            let obj = unsafe{self.hot_span(sc)}.allocate();
            if unlikely(obj.is_null()) {
                if unsafe{self.hot_span(sc)}.nr_free_objects() > CLASS_TO_REUSE_THRESHOLD.val_index(sc) as IFast32 {
                    unsafe{self.hot_span(sc)}.move_remote_to_local_objects();
                    unsafe{self.hot_span(sc)}.allocate()
                } else {
                    unsafe{self.hot_span(sc)}.new_mark_floating();
                    *self.hot_span.mut_index(sc) = self.get_span(sc as i32);
                    if unlikely(self.hot_span.val_index(sc).is_null()) {
                        unsafe{*libc::__errno_location() = libc::ENOMEM};
                        OptAddress::null()
                    } else {
                        unsafe{self.hot_span(sc)}.allocate()
                    }
                }
            } else {
                obj
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn free(&mut self, mut p: Address) {
            scalloc_assert_ne!(self.id(), *TERMINATED);
            let s = Span::from_object(p);
            if unlikely(unsafe{SEEN_MEMALIGN} != 0) {
                p = Address::from(s.align_to_block_start(Address::from(p)));
            }

            let old_epoch = s.epoch();
            let old_owner = s.owner();
            let size_class = s.size_class() as i32;
            let free_objects = s.free(p, self.id());

            let old_owner_value = unsafe{&mut *old_owner.value()};
            let old_owner = if (old_owner_value.id() == *TERMINATED
                || old_owner != old_owner_value.id()) && s.try_revive_new(old_owner, self.id()) {
                    s.try_mark_floating(old_epoch);
                    self.id()
            } else {
                old_owner
            };

            if !cfg!(feature = "SCALLOC_NO_CLEANUP_IN_FREE")
                    && unlikely(free_objects == CLASS_TO_OBJECTS.val_index(size_class as usize))
                    && Span::is_floating_or_reusable(old_epoch) && s.new_mark_full(old_epoch) {
                if Span::is_reusable(old_epoch) {
                    old_owner_value.r_spans.mut_index(size_class as usize).remove(old_owner, Shared::from(s.span_link()))
                }

                scalloc_assert!(!Span::is_hot(s.epoch()));
                Span::delete(s);
            } else if unlikely(free_objects > CLASS_TO_REUSE_THRESHOLD.val_index(size_class as usize))
                    && !Span::is_reusable(old_epoch)
                    //: For a terminated owner that is waiting we will still add it to the list
                    //: to keep the code paths simple. (Yep, that's overhead in this rare
                    //: case.)
                    && s.new_mark_reuse(old_epoch) {
                old_owner_value.r_spans.mut_index(size_class as usize).push_front(old_owner, Shared::from(s.span_link()))
            }
        }
    }
}
pub(crate) use self::core::Core;

mod guarded_core {
    use super::*;
    pub(super) type Lock = SpinLock64;
    pub(super) use spin_lock_64 as lock;
    #[repr(C)]
    pub(crate) struct GuardedCore {
        pub core: Core,
        pub(super) num_threads: AtomicUFast32,

        //: SW/MR.
        // volatile (i.e. only use read_volatile/write_volatile to access)
        pub(super) in_use: UFast32,

        pub(super) core_lock: Lock,
    }

    impl GuardedCore {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new(&mut self) -> &mut GuardedCore {
            Core::new(&mut self.core);
            self.num_threads = AtomicUFast32::new(0);
            self.in_use = 0;
            self.core_lock = Lock::new();
            self
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn allocate(&mut self, size: usize)->OptAddress {
             self.acquire();
            let p = if likely(self.num_threads.load(Ordering::SeqCst) == 1) {
                self.core.allocate(size)
            } else {
                self.allocate_locked(size)
            };
            self.release();
            p
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn free(&mut self, p: Address) {
            self.acquire();
            if likely(self.num_threads.load(Ordering::SeqCst) == 1) {
                self.core.free(p)
            } else {
                self.free_locked(p);
            };
            self.release();
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn allocate_locked(&mut self, size: usize)->OptAddress {
            let __guard = lock::Guard::new(&mut self.core_lock);
            self.core.allocate(size)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn free_locked(&mut self, p: Address) {
            let __guard = lock::Guard::new(&mut self.core_lock);
            self.core.free(p)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn in_use(&self)->bool { unsafe{read_volatile(&self.in_use) == 1} }
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn announce_new_thread(&mut self) { self.num_threads.fetch_add(1, Ordering::SeqCst); }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn acquire(&mut self) { unsafe { write_volatile(&mut self.in_use, 1) } }
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub(super) fn release(&mut self) { unsafe { write_volatile(&mut self.in_use, 0) } }
    }
}
pub(crate) use self::guarded_core::GuardedCore;

