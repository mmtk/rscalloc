use super::*;

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn malloc(size: usize) -> *mut Void {
    if cfg!(feature = "SCALLOC_NO_SAFE_GLOBAL_CONSTRUCTION") {
        if unlikely(unsafe{SCALLOC_GUARD_REF_COUNT == 0}) {
            unsafe{SCALLOC_GUARD_REF_COUNT += 1}
            scalloc_init();
        }
    }
    glue::malloc(size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn free(p: *mut Void) {
    glue::free(OptAddress::from(p))
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn cfree(p: *mut Void) {
    glue::free(OptAddress::from(p))
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn calloc(nmemb: usize, size: usize) -> *mut Void {
    glue::calloc(nmemb, size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn realloc(ptr: *mut Void, size: usize) -> *mut Void {
    glue::realloc(OptAddress::from(ptr), size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn memalign(alignment: usize, size: usize) -> *mut Void {
    glue::memalign(alignment, size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn aligned_alloc(alignment: usize, size: usize) -> *mut Void {
    glue::aligned_alloc(alignment, size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn posix_memalign(ptr: *mut *mut Void, align: usize, size: usize) -> Int {
    glue::posix_memalign(unsafe{transmute(ptr)}, align, size)
}
#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn valloc(__size: usize) -> *mut Void {
    glue::valloc(__size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn pvalloc(__size: usize) -> *mut Void {
    glue::pvalloc(__size).into()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn malloc_stats() {
    glue::malloc_stats()
}

#[linkage = "weak"]
#[no_mangle]
pub extern "C" fn mallopt(cmd: Int, value: Int) -> Int {
    glue::mallopt(cmd, value)
}