use super::*;

#[cfg(target_os = "linux")]
#[cfg(feature = "OVERRIDE_MALLOC")]
mod override_gcc_weak;
#[cfg(target_os = "linux")]
#[cfg(feature = "OVERRIDE_MALLOC")]
pub use self::override_gcc_weak::*;

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn replace_system_allocator() {}

mod pthread_intercept;
pub use self::pthread_intercept::*;