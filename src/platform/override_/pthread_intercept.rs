use super::*;

type StartFunc = extern "C" fn(*mut Void) -> *mut Void;
type PthreadCreateFunc = extern "C" fn(
    *mut libc::pthread_t,
    *mut libc::pthread_attr_t,
    StartFunc,
    *mut Void,
) -> Int;

#[derive(Copy, Clone)]
#[repr(C)]
pub struct ScallocStartArgs {
    pub real_start: StartFunc,
    pub real_args: *mut Void,
}
impl ScallocStartArgs {
    pub fn new(real_start: StartFunc, real_args: *mut Void) -> Self {
        Self {
            real_start: real_start,
            real_args: real_args,
        }
    }
}

#[no_mangle]
// UNSAFE: this is totally unsafe, it is treating the result of a dlsym as a
// function pointer (the result could be null, or it may not point to a
// function with the appropriate signature) in addition, it is not threadsafe
// due to the use of REAL_CREATE
pub unsafe extern "C" fn pthread_create(
    thread: *mut libc::pthread_t,
    attr: *mut libc::pthread_attr_t,
    start: StartFunc,
    arg: *mut Void,
) -> Int {
    static mut REAL_CREATE: Option<PthreadCreateFunc> = None;

    if REAL_CREATE.is_none() {
        REAL_CREATE = Some(transmute(libc::dlsym(
            libc::RTLD_NEXT,
            "pthread_create\0".as_bytes().as_ptr() as *const libc::c_char,
        )));
    }
    return unwrap!(REAL_CREATE)(
        thread,
        attr,
        scalloc_thread_start,
        Box::into_raw(Box::new(ScallocStartArgs::new(start, arg))) as *mut Void,
    );
}
