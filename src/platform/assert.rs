/*
#define DISALLOW_COPY_AND_ASSIGN(TypeName)                                     \
private:                                                                       \
  TypeName(const TypeName&);                                                   \
  void operator=(const TypeName&)
*/

/*#define DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName)                               \
private:                                                                       \
  TypeName();                                                                  \
  DISALLOW_COPY_AND_ASSIGN(TypeName)
*/

/*#define DISALLOW_ALLOCATION()                                                  \
public:                                                                        \
  void operator delete(void* pointer) {                                        \
    UNREACHABLE();                                                             \
  }                                                                            \
private:                                                                       \
  void* operator new(size_t size);
*/

#[cfg(feature = "SCALLOC_DEBUG")]
macro_rules! _scalloc_assert_inner {
    ($c:expr) => [ fatal!("assertion failed: ", stringify!($c)) ];
}

#[cfg(not(feature = "SCALLOC_DEBUG"))]
macro_rules! _scalloc_assert_inner {
    ($c:expr) => [ () ];
}

macro_rules! scalloc_assert {
    ($c:expr) => [if !$c {_scalloc_assert_inner!($c)} ];
}

#[cfg(feature = "SCALLOC_DEBUG")]
macro_rules! _scalloc_assert_eq_inner {
    ($left:expr, $right:expr, $left_val:ident, $right_val:ident) => [
        fatal!(concat!("assertion failed: ", stringify!($left),
            " == ", stringify!($right), "("),
            $left_val, " != ", $right_val, ")");
    ];
}
#[cfg(not(feature = "SCALLOC_DEBUG"))]
macro_rules! _scalloc_assert_eq_inner {
    ($left:expr, $right:expr, $left_val:ident, $right_val:ident) => [ () ];
}

macro_rules! scalloc_assert_eq {
    ($left:expr, $right:expr) => [
        // this trick evaluates the arguments only once
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(*left_val == *right_val) {
                    _scalloc_assert_eq_inner!($left, $right, left_val, right_val)
                }
            }
        }
    ];
}

#[cfg(feature = "SCALLOC_DEBUG")]
macro_rules! _scalloc_assert_ne_inner {
    ($left:expr, $right:expr, $left_val:ident, $right_val:ident) => [
        fatal!(concat!("assertion failed: ", stringify!($left),
            " != ", stringify!($right), "("),
            $left_val, " == ", $right_val, ")");
    ];
}
#[cfg(not(feature = "SCALLOC_DEBUG"))]
macro_rules! _scalloc_assert_ne_inner {
    ($left:expr, $right:expr, $left_val:ident, $right_val:ident) => [ () ];
}

macro_rules! scalloc_assert_ne {
    ($left:expr, $right:expr) => [
        // this trick evaluates the arguments only once
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(*left_val != *right_val) {
                    _scalloc_assert_ne_inner!($left, $right, left_val, right_val)
                }
            }
        }
    ];
}


/*macro_rules! unreachable {
  () => [fatal!("unreachable code segment")];
}*/