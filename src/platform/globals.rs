use super::*;

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn likely(b: bool) -> bool {
    unsafe { std::intrinsics::likely(b) }
}

#[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
#[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
pub fn unlikely(b: bool) -> bool {
    unsafe { std::intrinsics::unlikely(b) }
}

//macro_rules! always_inline { () => [#[cfg_attr(feature = "SCALLOC_INLINE", inline(always))]]; }

#[allow(dead_code)] // Rustc you are an idiot (this is used in src/span.rs)
pub const CACHE_LINE_SIZE: i32 = 64;

// Has an alignment of CACHE_LINE_SIZE (i.e. 64-bytes)
aligned!(pub type CacheAligned(u64, u64, u64, u64, u64, u64, u64, u64));

//#define UNUSED __attribute__((unused))

//: Generally use TLS.
#[cfg(not(target_os = "macos"))]
pub const HAVE_TLS: bool = true;

//: TLS uses malloc() on OSX, so we have to use TSD.
#[cfg(target_os = "macos")]
pub const HAVE_TLS: bool = false;

//#define TLS_ATTRIBUTE __thread

//#ifdef __APLE__
//      #ifndef MAP_ANONYMOUS:
//          #define MAP_ANONYMOUS MAP_ANON
