// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

pub mod span {
    use super::*;
    pub const ALIGN_TAG: IFast32 = 0xAAAAAAAA;

    #[repr(C)]
    #[repr(C)]
    pub(crate) struct Span {
        //: This list is used to link up reusable spans in the corresponding core. The
        //: first word is also used in the span pool to link up spans.
        span_link: DoubleListNode,

        owner: AtomicCoreID,

        //: Epoch counter that even survives traversing of a span through the span
        //: pool.
        epoch: AtomicI32,
        size_class: i32,
        __padding: [Byte; 8],
        local_free_list: IncrementalFreeList,
        remote_free_list: RemoteFreeList,
    }
    impl Span {
        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn is_floating_or_reusable(epoch: i32) -> bool {
            !Self::is_full(epoch) && !Self::is_hot(epoch)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn is_reusable(epoch: i32) -> bool {
            (epoch & EPOCH_REUSE) != 0
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn is_hot(epoch: i32) -> bool {
            (epoch & EPOCH_HOT) != 0
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn is_full(epoch: i32) -> bool {
            (epoch & EPOCH_FULL) != 0
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn nr_free_objects(&self) -> IFast32 {
            self.nr_local_objects() + self.nr_remote_objects()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn nr_remote_objects(&self) -> IFast32 {
            if self.remote_free_list.empty() {
                0
            } else {
                self.remote_free_list.length()
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn nr_local_objects(&self) -> IFast32 {
            self.local_free_list.length()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn from_object<'a>(p: Address)->&'a mut Span {
            unsafe{(p & VIRTUAL_SPAN_MASK).to_mut()}
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn from_span_link<'a>(link: Shared<DoubleListNode>) -> &'a mut Span {
            unsafe{Address::from(link).to_mut()}
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new(size_class: usize, owner: CoreId)->*mut Span {
            let span = unsafe{SPAN_POOL.allocate(size_class, owner.tag() as i32).to_mut::<Span>()};
            span.init(size_class, owner);
            span
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn delete(s: &mut Span) {
            scalloc_assert!(s.span_link.is_empty());
            // Note: this is precomputed here, since Address::from(s) moves s
            let tag = s.owner().tag() as i32;
            unsafe{SPAN_POOL.free(s.size_class(), Address::from(s), tag)};
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn align_to_block_start(&self, p: Address) -> Address {
            if *unsafe{(p - size_of::<IFast32>()).to_ref::<IFast32>()} == ALIGN_TAG {
                let d = (p - self.header_end() as UIntPtr).to_unsigned()
                    % (CLASS_TO_SIZE.val_index(self.size_class as usize) as UIntPtr);
                warning!("found aligned adr: ", p);
                let p = p - d;
                warning!("  fix to: ", p);
                p
            } else {
                p
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn init(&mut self, size_class: usize, owner: CoreId) {
            self.span_link = DoubleListNode::default();
            self.owner = AtomicCoreID::new(&owner);
            self.size_class = size_class as i32;
            self.local_free_list = IncrementalFreeList::new(self.header_end(), size_class);
            self.remote_free_list = RemoteFreeList::new();

            scalloc_assert_eq!(self.local_free_list.length(),
                CLASS_TO_OBJECTS.val_index(size_class) as IFast32);

            scalloc_assert_eq!(self.remote_free_list.length(), 0);
            scalloc_assert!(!owner.value().is_null());

            // Mark span as hot.
            let epoch = self.epoch();
            self.new_mark_hot(epoch);

            if cfg!(feature = "SCALLOC_DEBUG") {
                self.check_alignments()
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn size_class(&self) -> usize {
            self.size_class as usize
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn owner(&self) -> CoreId {
            self.owner.load()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn epoch(&self) -> i32 {
            self.epoch.load(Ordering::SeqCst)
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn try_revive_new(&mut self, old_owner: CoreId, caller: CoreId) -> bool {
            scalloc_assert!(!caller.value().is_null());
            let ok = self.owner.swap(&old_owner, &caller);
            ok
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn header_end(&self) -> IntPtr {
            (Address::from(self) + size_of::<Self>()).into()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn span_link(&mut self) -> &mut DoubleListNode {
            // Note: this trick is needed, as Address::from(self) moves self
            let res = Address::from(&self.span_link);
            scalloc_assert_eq!(res, Address::from(self));
            unsafe{res.to_mut()}
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new_mark_hot(&mut self, old_epoch: i32) -> bool {
            let old_epoch = old_epoch & EPOCH_REUSE_MASK;
            //: We want to set only the hot bit and a new value.
            let new_epoch = ((old_epoch + 1) | EPOCH_HOT) & EPOCH_HOT_MASK;
            self.epoch
                .compare_and_swap(old_epoch, new_epoch, Ordering::SeqCst) == old_epoch
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new_mark_full(&mut self, old_epoch: i32) -> bool {
            //: Reuse bit can be set, but nothing else.
            let old_epoch = old_epoch & EPOCH_REUSE_MASK;
            let new_epoch = ((old_epoch + 1) | EPOCH_FULL) & EPOCH_FULL_MASK;
            self.epoch
                .compare_and_swap(old_epoch, new_epoch, Ordering::SeqCst) == old_epoch
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new_mark_reuse(&mut self, old_epoch: i32) -> bool {
            //: None of the bits should be set.
            let old_epoch = old_epoch & EPOCH_ONLY_VALUES_MASK;
            let new_epoch = ((old_epoch + 1) | EPOCH_REUSE) & EPOCH_REUSE_MASK;
            self.epoch
                .compare_and_swap(old_epoch, new_epoch, Ordering::SeqCst) == old_epoch
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn new_mark_floating(&mut self) {
            //: There's no race in this one as we always go through the hot state which
            //: is already exclusive.
            self.epoch.store(
                self.epoch.load(Ordering::SeqCst) & EPOCH_ONLY_VALUES_MASK,
                Ordering::SeqCst,
            )
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn try_mark_floating(&mut self, old_epoch: i32) -> bool {
            let new_epoch = old_epoch & EPOCH_ONLY_VALUES_MASK;
            self.epoch
                .compare_and_swap(old_epoch, new_epoch, Ordering::SeqCst) == old_epoch
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        fn check_alignments(&self) {
            fn check_field<T>(field: &T, name: &str) {
                if !(OptAddress::from(field) & 0x3).is_null() {
                    fatal!("Field ", name, " not properly aligned.");
                }
            }

            check_field(&self.span_link, "span_link");
            check_field(&self.owner, "owner");
            check_field(&self.epoch, "epoch");
            check_field(&self.size_class, "size_class");
            check_field(&self.local_free_list, "local_free_list");
            check_field(&self.remote_free_list, "remote_free_list");

            fn check_cache_aligned_adr(adr: OptAddress, id: &str) {
                if !(adr % (CACHE_LINE_SIZE as usize)).is_null() {
                    fatal!("Address ", id, " not ", CACHE_LINE_SIZE, " byte aligned: ", adr);
                }
            }
            check_cache_aligned_adr(OptAddress::from(&self.remote_free_list), "remote_free_list");
            check_cache_aligned_adr(OptAddress::from(self.header_end()), "remote_free_list");
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn allocate(&mut self) -> OptAddress {
            self.local_free_list.pop()
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn free(&mut self, p: Address, caller: CoreId) -> i32 {
            if self.owner() == caller { //: Local free.
                #[cfg(feature = "profile")] {
                    unsafe{LOCAL_FREES.fetch_add(1, Ordering::SeqCst)};
                }
                ((self.local_free_list.push(p) as IFast32) + self.nr_remote_objects()) as i32
            } else { //: Remote free.
                #[cfg(feature = "profile")] {
                    unsafe{REMOTE_FREES.fetch_add(1, Ordering::SeqCst)};
                }
                ((self.remote_free_list.push(p) as IFast32) + self.nr_local_objects()) as i32
            }
        }

        #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
        #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
        pub fn move_remote_to_local_objects(&mut self) {
            if self.nr_remote_objects() != 0 {
                let (objects, actual_len) = self.remote_free_list.pop_all();
                let actual_len = actual_len as i32;
                if self.nr_local_objects() == 0 {
                    self.local_free_list.set_list(objects, actual_len as usize)
                } else {
                    let mut objects = objects;
                    let mut count = 0i32;
                    while !objects.is_null() {
                        let next = unsafe{*objects.to_ref::<OptAddress>()};
                        count += 1;
                        self.local_free_list.push(unwrap!(objects));
                        objects = next;
                    }

                    scalloc_assert_eq!(count, actual_len);
                    trace!("[", self.owner().tag(), "] have ",
                        self.nr_local_objects(), " local objs"
                    );
                }
            }
        }
    }

    type RemoteFreeList = Stack64;

    // EpochBit
    const HOT_BIT: Int = 31;
    const FULL_BIT: Int = 30;
    const REUSE_BIT: Int = 29;
    const LAST_VALUE_BIT: Int = 28;

    // EpochValue
    const EPOCH_HOT: Int = (1 << HOT_BIT);
    const EPOCH_FULL: Int = (1 << FULL_BIT);
    const EPOCH_REUSE: Int = (1 << REUSE_BIT);

    // EpochMask
    const EPOCH_ONLY_VALUES_MASK: Int = (1 << LAST_VALUE_BIT) - 1;
    const EPOCH_HOT_MASK: Int = EPOCH_ONLY_VALUES_MASK | EPOCH_HOT;
    const EPOCH_FULL_MASK: Int = EPOCH_ONLY_VALUES_MASK | EPOCH_FULL;
    const EPOCH_REUSE_MASK: Int = EPOCH_ONLY_VALUES_MASK | EPOCH_REUSE;
}
pub(crate) use self::span::Span;
