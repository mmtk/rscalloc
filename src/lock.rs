// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;
// use log::*;
// use platform::globals::*;

pub trait SpinLock {
    fn _lock(&mut self) -> &mut AtomicUFast32;

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn new()->Self;

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn lock(&mut self) {
        while !self.try_lock() {
            // SAFE: this instruction does nothing except alter performance (neccesary for
            // an efficient spin-lock)
            unsafe { asm!("PAUSE") };
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn try_lock(&mut self) -> bool { self._lock().swap(1, Ordering::SeqCst) == 0 }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn unlock(&mut self) { self._lock().store(0, Ordering::SeqCst); }
}

pub mod spin_lock_0 {
    use super::*;
    pub type Guard = LockHolder<SpinLock0>;
    #[repr(C)]
    pub struct SpinLock0 {
        lock: AtomicUFast32,
    }
    impl SpinLock0 {
        pub const fn new() -> Self { Self { lock: AtomicUFast32::new(0) } }
    }
    impl SpinLock for SpinLock0 {
        fn new() -> Self { Self { lock: AtomicUFast32::new(0) } }
        fn _lock(&mut self) -> &mut AtomicUFast32 { &mut self.lock }
    }
}
pub use spin_lock_0::SpinLock0;
pub mod spin_lock_64 {
    use super::*;
    pub type Guard = LockHolder<SpinLock64>;
    #[repr(C)]
    pub struct SpinLock64 {
        lock: AtomicUFast32,
        __padding: [u8; 64 - size_of::<AtomicUFast32>()]
    }
    impl SpinLock for SpinLock64 {
        fn new() -> Self { Self { lock: AtomicUFast32::new(0), __padding: unsafe{uninitialized()} } }
        fn _lock(&mut self) -> &mut AtomicUFast32 { &mut self.lock }
    }
}
pub use spin_lock_64::SpinLock64;

#[repr(C)]
pub struct LockHolder<LockType: SpinLock> {
    lock: *mut LockType,
}
impl<LockType: SpinLock> LockHolder<LockType> {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn new(lock: &mut LockType) -> Self {
        let this = Self { lock: lock };
        lock.lock();
        this
    }
}
impl<LockType: SpinLock> Drop for LockHolder<LockType> {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn drop(&mut self) {
        unsafe{&mut*self.lock}.unlock();
    }
}