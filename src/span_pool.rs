// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

const SIZE_CLASS_SLOTS: i32 = (COARSE_CLASSES + 1) as i32;
const HARD_LIMIT: i32 = dflag!(SCALLOC_SPAN_POOL_BACKEND_LIMIT = 16384);
type Backend = Stack64;

#[repr(C)]
pub struct SpanPool {
    //: The currently announced number of threads.
    current_threads: AtomicI32,
    //: A monotonically increasing limit of backends that depends on the number of
    //: threads.
    limit: AtomicI32,
    __pad: [u8; 64 - (2 * size_of::<AtomicI32>()) % 64],
    spans: [*mut Backend; SIZE_CLASS_SLOTS as usize],

    #[cfg(feature = "PROFILE")] nr_allocate: AtomicI32,
    #[cfg(feature = "PROFILE")] nr_free: AtomicI32,
    #[cfg(feature = "PROFILE")] nr_madvise: AtomicI32,
}

impl SpanPool {
    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    unsafe fn get_span<'a>(&self, index1: usize, index2: usize)->&'a mut Backend {
        &mut*self.spans.val_index(index1).add(index2)
    }


    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    //: Globally constructed, hence we use staged construction.
    #[cfg(feature = "PROFILE")]
    pub const fn new() -> Self {
        // TODO unsafe{uninitialized()}
        Self {
            current_threads: AtomicI32::new(0),
            limit: AtomicI32::new(0),
            __pad: [0; 64 - (2 * size_of::<AtomicI32>()) % 64],
            spans: [null_mut(); SIZE_CLASS_SLOTS as usize],

            nr_allocate: AtomicI32::new(0),
            nr_free: AtomicI32::new(0),
            nr_madvise: AtomicI32::new(0),
        }
    }

    #[cfg(not(feature = "PROFILE"))]
    pub const fn new() -> Self {
        // TODO unsafe{uninitialized()}
        Self {
            current_threads: AtomicI32::new(0),
            limit: AtomicI32::new(0),
            __pad: [0; 64 - (2 * size_of::<AtomicI32>()) % 64],
            spans: [null_mut(); SIZE_CLASS_SLOTS as usize],
        }
    }

    #[cfg(feature = "PROFILE")]
    #[inline]
    pub fn print_profile_summary(&self) {
        warning!("span pool: allocations: ", self.nr_allocate.load(Ordering::SeqCst),
            ",  deallocations: ", self.nr_free.load(Ordering::SeqCst))
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    fn limit(&self) -> i32 {
        self.limit.load(Ordering::SeqCst)
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn init(&mut self) {
        self.current_threads = AtomicI32::new(0);
        self.limit = AtomicI32::new(0);

        #[cfg(feature = "profile")] {
            self.nr_allocate = AtomicI32::new(0);
            self.nr_free = AtomicI32::new(0);
            self.nr_madvise = AtomicI32::new(0);
        }

        for i in 0..SIZE_CLASS_SLOTS as usize {
            *self.spans.mut_index(i) = system_mmap_fail(size_of::<Backend>() * cpus_online() as usize).into()
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn announce_new_thread(&mut self) {
        let cpus = cpus_online() as IFast32;
        let new_limit = self.current_threads.fetch_add(1, Ordering::SeqCst) + 1;
        if (new_limit as IFast32 <= cpus) && (new_limit <= HARD_LIMIT) {
            let mut old_limit: i32;
            do_!{{
                old_limit = self.limit();
            } while (new_limit > old_limit) &&
                !self.limit.compare_exchange_weak(old_limit, old_limit + 1, Ordering::SeqCst,
                    Ordering::SeqCst).is_ok()
            };
        }
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn announce_leaving_thread(&mut self) {
        self.current_threads.fetch_sub(1, Ordering::SeqCst);
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn allocate(&mut self, size_class: usize, id: i32) -> OptAddress {
        #[cfg(feature = "profile")] {
            self.nr_allocate.fetch_add(1, Ordering::SeqCst);
        }

        trace!("allocate size class: ", size_class, ", limit: ", self.limit(), " id: ", id);

        scalloc_assert_ne!(self.limit(), 0);

        let size_class_slot = if size_class < FINE_CLASSES {
            0
        } else {
            size_class - FINE_CLASSES
        };

        let mut i = size_class_slot as i32;
        let mut s = unsafe{self.get_span(size_class_slot, (id % self.limit()) as usize)}.pop();

        for j in 0..SIZE_CLASS_SLOTS as usize {
            if !s.is_null() { break; }

            i = (size_class_slot.wrapping_sub(j)) as i32;
            if i < 0 {
                i += SIZE_CLASS_SLOTS
            };

            let start = (hwrand() % self.limit() as UFast64) as u64;
            trace!("start: ", start);
            for k in 0..(self.limit() as IFast32) {
                if !s.is_null() { break; }
                s = unsafe{self.get_span(i as usize, ((start + k as u64) % self.limit() as u64) as usize)}.pop();
            }
        }

        if s.is_null() {
            s = unsafe{&mut OBJECT_SPACE}.allocate_virtual_span().into();
        } else {
            if cfg!(feature = "SCALLOC_MADVISE") && !cfg!(feature = "SCALLOC_MADVISE_EAGER") {
                //: madvise for any of the non-fine size classes
                if (i > 0) && CLASS_TO_SPAN_SIZE.val_index(i as usize + FINE_CLASSES) > CLASS_TO_SPAN_SIZE.val_index(size_class) {
                    unsafe{libc::madvise((s + CLASS_TO_SPAN_SIZE.val_index(size_class) as usize).into(),
                        VIRTUAL_SPAN_SIZE - CLASS_TO_SPAN_SIZE.val_index(size_class) as usize,
                        libc::MADV_DONTNEED,
                    )};

                    #[cfg(feature = "profile")] {
                        self.nr_madvise.fetch_add(1, Ordering::SeqCst);
                    }
                }
            }
        }

        if cfg!(feature = "SCALLOC_STRICT_PROTECT = []") {
            if unsafe{libc::mprotect(s.into(), CLASS_TO_SPAN_SIZE.val_index(size_class as usize) as usize,
                libc::PROT_READ | libc::PROT_WRITE)} != 0 {
                fatal!("mprotect failed");
            }
        }

        s
    }

    #[cfg_attr(not(feature = "SCALLOC_NOINLINE"), inline(always))]
    #[cfg_attr(feature = "SCALLOC_NOINLINE", inline(never))]
    pub fn free(&mut self, size_class: usize, p: Address, id: i32) {
        #[cfg(feature = "profile")]{
            self.nr_free.fetch_add(1, Ordering::SeqCst);
        }

        trace!("span pool put ", p, ", size class: ", size_class);
        if cfg!(feature = "SCALLOC_MADVISE") && cfg!(feature = "SCALLOC_MADVISE_EAGER") {
            if size_class >= 17 {
                unsafe{libc::madvise(
                    (p + PAGE_SIZE).into(),
                    VIRTUAL_SPAN_SIZE - PAGE_SIZE,
                    libc::MADV_DONTNEED,
                )};

                #[cfg(feature = "profile")] {
                    self.nr_madvise.fetch_add(1, Ordering::SeqCst);
                }
            }
        }

        let size_class = if size_class <= FINE_CLASSES {
            0
        } else {
            size_class - FINE_CLASSES
        };

        if cfg!(feature = "SCALLOC_STRICT_PROTECT") {
            if unsafe{libc::mprotect(
                    (p + PAGE_SIZE).into(),
                    VIRTUAL_SPAN_SIZE - PAGE_SIZE,
                    libc::PROT_NONE)} != 0 {
                fatal!("mprotect failed");
            }
        }

        unsafe{self.get_span(size_class, (id % self.limit()) as usize)}.push(p);
    }
}
