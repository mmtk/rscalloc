// Copyright (c) 2017, The Australian National University.  All rights reserved.
// Based on work copyright (c) 2012-2015, the scalloc project authors. All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.
use super::*;

macro_rules! address_methods {
	() => [
		#[inline(always)]
		pub fn to_unsigned(self) -> UIntPtr { self.to_void_ptr() as UIntPtr }

		#[inline(always)]
		pub fn to_signed(self) -> IntPtr { self.to_void_ptr() as IntPtr }

        #[inline(always)]
        pub fn to_void_ptr(self) -> *mut Void { self.to_ptr::<Void>() }

		#[inline(always)]
		pub unsafe fn to_mut<'a, T>(self) -> &'a mut T { unwrap!(self.to_ptr::<T>().as_mut()) }

		#[inline(always)]
		pub unsafe fn to_ref<'a, T>(self) -> &'a T { unwrap!(self.to_ptr::<T>().as_ref()) }

		#[inline(always)]
		pub unsafe fn load<T>(&self) -> T { self.to_ptr::<T>().read() }

		#[inline(always)]
		pub unsafe fn store<T>(&mut self, v: T) { self.to_ptr::<T>().write(v) }
	];
}

macro_rules! binop_impl {
    ($A:ident $Trait:ident<usize>.$F:ident) => [
        impl $Trait<usize> for $A {
            type Output = $A;
            #[inline(always)]
            fn $F(self, other: usize) -> $A { $A::from(self.to_unsigned().$F(other)) }
        }
    ];
    ($A:ident $Trait:ident<isize>.$F:ident) => [
        impl $Trait<isize> for $A {
            type Output = $A;
            #[inline(always)]
            fn $F(self, other: isize) -> $A { $A::from(self.to_signed().$F(other)) }
        }
    ];
}

macro_rules! address_impl {
	($A:ident) => [
	    binop_impl!($A Div<usize>.div);
		binop_impl!($A Rem<usize>.rem);
		binop_impl!($A BitAnd<usize>.bitand);
		binop_impl!($A BitOr<usize>.bitor);
		binop_impl!($A BitXor<usize>.bitxor);
		binop_impl!($A Add<usize>.add);
		binop_impl!($A Add<isize>.add);
		binop_impl!($A Sub<usize>.sub);
		binop_impl!($A Sub<isize>.sub);

		impl Sub<$A> for $A {
			type Output = isize;
			#[inline(always)]
			fn sub(self, other: $A) -> isize { self.to_signed() - other.to_signed() }
		}

		impl From<UIntPtr> for $A {
		    #[inline(always)]
			fn from(val: UIntPtr)->$A { $A::new(val as *mut Void) }
		}
		impl From<IntPtr> for $A {
            #[inline(always)]
			fn from(val: IntPtr)->$A { $A::new(val as *mut Void) }
		}
		impl<T> From<*mut T> for $A {
		    #[inline(always)]
			fn from(val: *mut T)->$A { $A::new(val) }
		}
		impl<T> From<*const T> for $A {
		    #[inline(always)]
			fn from(val: *const T)->$A { $A::new(val) }
		}
		impl<'a, T> From<&'a T> for $A {
		    #[inline(always)]
			fn from(val: &'a T)->$A { $A::new(val as *const T) }
		}
		impl<'a, T> From<&'a mut T> for $A {
		    #[inline(always)]
			fn from(val: &'a mut T)->$A { $A::new(val as *const T) }
		}
		impl<T> From<Shared<T>> for $A {
		    #[inline(always)]
			fn from(val: Shared<T>)->$A { $A::new(val.as_ptr()) }
		}
        impl<T> From<Option<Shared<T>>> for $A {
            #[inline(always)]
            fn from(val: Option<Shared<T>>)->$A {
                $A::new(val.map_or(null_mut(), |x| x.as_ptr()))
            }
        }

		impl Into<UIntPtr> for $A {
		    #[inline(always)]
			fn into(self)->UIntPtr {
				self.to_void_ptr() as UIntPtr
			}
		}
		impl Into<IntPtr> for $A {
		    #[inline(always)]
			fn into(self)->IntPtr {
				self.to_void_ptr() as IntPtr
			}
		}
		impl<T> Into<*mut T> for $A {
		    #[inline(always)]
			fn into(self)->*mut T {
				self.to_void_ptr() as *mut T
			}
		}
        impl<T> Into<*const T> for $A {
            #[inline(always)]
			fn into(self)->*const T {
				self.to_void_ptr() as *const T
			}
		}
		// Note: we don't implement Into for references as this is unsafe
		// (use to_ref or to_mut instead)
		impl Eq for $A { }
		impl PartialEq<$A> for $A {
			#[inline(always)]
			fn eq(&self, other: &$A)->bool {
				self.to_void_ptr() == other.to_void_ptr()
			}
		}

		impl Ord for $A {
			#[inline(always)]
			fn cmp(&self, other: &Self) -> std::cmp::Ordering { self.to_void_ptr().cmp(&other.to_ptr()) }
		}
		impl PartialOrd for $A {
			#[inline(always)]
			fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> { Some(self.cmp(other)) }
		}
	];
}

impl PartialEq<OptAddress> for Address {
    #[inline(always)]
    fn eq(&self, other: &OptAddress)->bool { self.to_void_ptr() == other.to_void_ptr() }
}

impl PartialEq<Address> for OptAddress {
    #[inline(always)]
    fn eq(&self, other: &Address)->bool { self.to_void_ptr() == other.to_void_ptr() }
}